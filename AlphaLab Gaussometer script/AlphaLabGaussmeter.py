'''
AlphaLab gaussmeter module

There is room for supporting more of the meter functions, but the basics
contained in this file, were all that was needed for the project.

Keep in mind, that the basic parameters of your meter may differ from ours, and you should
take a look in your meter's communication protocols before using this script.

Authors:

Rafał Osadnik

Silesian University of Technology
Institute of Physics

Simulation done for:

Helmholtz Coils Clinostat PBL POLSL

To keep up to date, visit our fanpage at: www.facebook.com/helmoltzpolsl/

------------------------------------------------------------------------------------------------------------------------

Supported by EU project: Projekt wdrożeniowy POWER 3.5 p.t. "Politechnika Śląska jako Centrum Nowoczesnego Kształcenia
opartego o badania i innowacje" (POWR.03.05.00-IP.08-00-PZ1/17), finansowany z Funduszy Europejskich Programu Operacyjnego
Wiedza Edukacja Rozwój (PO WER 3.5).

'''


import serial
import serial.tools.list_ports
import time
import struct
import numpy as np


class Gaussmeter:

    SLEEP_TIME = 0.25
    DATA_PACKET_SIZE = 6 # Value in bytes, given in the meter's manual
    INFO_DATA_PACKET = 20 # Same as above
    ACK_TERMINATE_BYTE = 1

    def __init__(self, serialPort):

        self.port = serial.Serial(serialPort,115200,timeout = 2)
        self.FIRST_STREAM = True

    def DisplayProperties(self):

        PROP_COMMAND = serial.to_bytes([0x01, 0x03, 0x03, 0x03, 0x03, 0x03])
        CONTINUE_COMMAND = serial.to_bytes([0x08,0x08, 0x08,0x08, 0x08,0x08])

        Data = []

        self.port.write(PROP_COMMAND)

        time.sleep(self.SLEEP_TIME)

        while True:

            Data.append(self.port.read(self.INFO_DATA_PACKET))
            Continue = self.port.read(self.ACK_TERMINATE_BYTE)

            if Continue == b'\x07':
                break
            else:
                self.port.write(CONTINUE_COMMAND)

        print([Data[i] for i in range(len(Data))])

    def DisplaySettings(self):

        SETT_COMMAND = serial.to_bytes([0x02, 0x03, 0x03, 0x03, 0x03, 0x03])
        CONTINUE_COMMAND = serial.to_bytes([0x08, 0x08, 0x08, 0x08, 0x08, 0x08])

        Data = []

        self.port.write(SETT_COMMAND)

        time.sleep(self.SLEEP_TIME)

        while True:

            Data.append(self.port.read(self.INFO_DATA_PACKET))
            Continue = self.port.read(self.ACK_TERMINATE_BYTE)

            if Continue == b'\x07':
                break
            else:
                self.port.write(CONTINUE_COMMAND)

        print([Data[i] for i in range(len(Data))])

    def StreamData(self):

        if self.FIRST_STREAM:

            STREAM_COMMAND = serial.to_bytes([0x04, 0x03, 0x03, 0x03, 0x03, 0x03])

        else:

            STREAM_COMMAND = serial.to_bytes([0x03, 0x03, 0x03, 0x03, 0x03, 0x03])

        self.port.write(STREAM_COMMAND)
        Data = []
        time.sleep(self.SLEEP_TIME)

        if self.FIRST_STREAM:
            AdditionalByte = self.port.read(1) # Additional byte that is not mentioned in the manual
            self.FIRST_STREAM = False

        for i in range(5):

            Data.append(self.port.read(self.DATA_PACKET_SIZE))

        while True:

            if self.port.in_waiting:

                ReadyToStream= self.port.read(self.ACK_TERMINATE_BYTE)

                if ReadyToStream == b'\x08':
                    break

        return Data


    def AlterStreamPeriod(self,index):

        if index > 9:
            index = 9

        AvailableFrequencies = [0,1,2,5,10,20,30,40,60,120]
        AlterCommand = serial.to_bytes([0x0E,0x0F,0x01,0x01,0b00000000,AvailableFrequencies[index]])
        self.port.write(AlterCommand)

        time.sleep(self.SLEEP_TIME)

        while True:

            ACK = self.port.read(1)

            if ACK == b'\x08':
                break


    def BreakSuspend(self):

        BS_COMMAND = serial.to_bytes([0x14, 0x01, 0x01, 0x01, 0x01, 0x01])
        self.port.write(BS_COMMAND)

def DecodeDataPacket(packet):

    binConfigBytes = [format(byte,'08b') for byte in packet[:2]]

    ConfigString = binConfigBytes[0] + binConfigBytes[1]
    NullBit = bool(int(ConfigString[1]))
    FieldBits = [bool(int(ConfigString[2])), bool(int(ConfigString[3]))]
    H1Bit = bool(int(ConfigString[4]))
    H2Bit = bool(int(ConfigString[5]))
    WarningBit = bool(int(ConfigString[6]))

    if ~NullBit:

        if ConfigString[12] == '1':
            Sign = -1
        else:
            Sign = 1

        Decimal = int(ConfigString[-3:],2)
        DataValue = 0

        for i,byte in zip(range(24,-1,-8),packet[2:]):

            DataValue += byte << i

        return DataValue * Sign / 10**Decimal

    else:
        return 'INVALID_VALUE'
