import serial
import serial.tools.list_ports
import time
import struct
import numpy as np


class Gaussmeter:

    def __init__(self,serialPort):

        self.port = serial.Serial(serialPort,115200,timeout = 2)
        self.FirstStream = True

    def DisplayProperties(self):

        IdCommand = serial.to_bytes([0x01, 0x03, 0x03, 0x03, 0x03, 0x03])
        ContinueCommand = serial.to_bytes([0x08,0x08, 0x08,0x08, 0x08,0x08])

        Data = []

        self.port.write(IdCommand)

        while True:

            Data.append(self.port.read(20))
            Continue = self.port.read(1)

            if Continue == b'\x07':
                break
            else:
                self.port.write(ContinueCommand)

        print([Data[i] for i in range(len(Data))])

    def DisplaySettings(self):

        SettingsCommand = serial.to_bytes([0x02, 0x03, 0x03, 0x03, 0x03, 0x03])
        ContinueCommand = serial.to_bytes([0x08, 0x08, 0x08, 0x08, 0x08, 0x08])

        Data = []

        self.port.write(SettingsCommand)

        while True:

            Data.append(self.port.read(20))
            Continue = self.port.read(1)

            if Continue == b'\x07':
                break
            else:
                self.port.write(ContinueCommand)

        print([Data[i] for i in range(len(Data))])

    def StreamData(self):

        if self.FirstStream:

            StreamCommand = serial.to_bytes([0x04, 0x03, 0x03, 0x03, 0x03, 0x03])

        else:

            StreamCommand = serial.to_bytes([0x03, 0x03, 0x03, 0x03, 0x03, 0x03])

        self.port.write(StreamCommand)
        Data = []
        time.sleep(0.5)

        if self.FirstStream:

            AdditionalByte = self.port.read(1) # Additional byte that is not mentioned in the manual
            self.FirstStream = False

        for i in range(5):

            Data.append(self.port.read(6))

        while True:

            if self.port.in_waiting:

                ReadyToStream= self.port.read(1)

                if ReadyToStream == b'\x08':
                    break

        return Data


    def DecodeDataPacket(self,packet):

        binConfigBytes = [format(byte,'08b') for byte in packet[:2]]

        ConfigString = binConfigBytes[0] + binConfigBytes[1]
        NullBit = bool(int(ConfigString[1]))
        FieldBits = [bool(int(ConfigString[2])), bool(int(ConfigString[3]))]
        H1Bit = bool(int(ConfigString[4]))
        H2Bit = bool(int(ConfigString[5]))
        WarningBit = bool(int(ConfigString[6]))

        if ~NullBit:

            if ConfigString[12] == '1':
                Sign = -1
            else:
                Sign = 1

            Decimal = int(ConfigString[-3:],2)
            DataValue = 0

            for i,byte in zip(range(24,-1,-8),packet[2:]):

                DataValue += byte << i

            return DataValue * Sign / 10**Decimal
        else:
            return 0


    def AlterStreamPeriod(self,index):

        AvailableFrequencies = [0,1,2,5,10,20,30,40,60,120]
        AlterCommand = serial.to_bytes([0x0E,0x0F,0x01,0x01,0b00000000,AvailableFrequencies[index]])
        self.port.write(AlterCommand)
        ACK = self.port.read(1)

METER = Gaussmeter('/dev/ttyUSB0')

METER.AlterStreamPeriod(2)
time.sleep(0.5)
METER.DisplaySettings()
time.sleep(0.5)

a = METER.StreamData()
print(METER.DecodeDataPacket(a[1]))

time.sleep(0.5)

while True:
    a = METER.StreamData()
    print(METER.DecodeDataPacket(a[1]))
    time.sleep(0.5)

'''

while True:

    b = METER.StreamData()
    print(b)
'''
