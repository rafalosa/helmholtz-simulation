"""
Before using this script make sure to establish a fixed port for your meter.
In order to do this, you need to know your meter serial number.
You can get your device's serial number by running:

sudo udevadm info --query=property --name=/dev/ttyUSB0 | grep SERIAL

After obtaining the serial number for your device, you have to add it to your usb rules file

To create a new rule file,or open an existing one run:

sudo nano /etc/udev/rules.d/99-usbserial.rules

and add the following lines to your file:

ACTION=="add",ENV{ID_BUS}=="usb",ENV{ID_SERIAL_SHORT}=="serial_number",SYMLINK+="ttyUSBGauss"
ACTION=="add", RUN="/bin/chmod a+rw /dev/ttyUSBGauss"

replacing serial_number with an actual serial number of your device.

Then save and exit.

"""

import AlphaLabGaussmeter as gm
import time

Meter1 = gm.Gaussmeter('/dev/ttyUSBGauss')

Meter1.AlterStreamPeriod(7)

Meter1.BreakSuspend()

Meter1.DisplaySettings()

Meter1.DisplayProperties()

while True:

    Data = Meter1.StreamData()

    print([gm.DecodeDataPacket(packet) for packet in Data])

    time.sleep(0.1)

