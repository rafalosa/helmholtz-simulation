import serial
import serial.tools.list_ports
import time

def DecodeDataPacket(packet):

    binConfigBytes = [format(byte,'08b') for i, byte in zip(range(2), packet)]

    ConfigString = binConfigBytes[0] + binConfigBytes[1]

    NullBit = bool(int(ConfigString[1]))
    FieldBits = [bool(int(ConfigString[2])), bool(int(ConfigString[3]))]
    H1Bit = bool(int(ConfigString[4]))
    H2Bit = bool(int(ConfigString[5]))
    WarningBit = bool(int(ConfigString[6]))

    if ConfigString[12] == '1':
        Sign = -1
    else:
        Sign = 1

    Decimal = int(ConfigString[-3:],2)

    MSB = packet[2] << 24
    B1 = packet[3] << 16
    B2 = packet[4] << 8
    LSB = packet[5]

    DataValue = Sign * (MSB + B1 + B2 + LSB) / 10**Decimal

    return DataValue



IdCommand = serial.to_bytes([0x01, 0x03, 0x03, 0x03, 0x03, 0x03])
SettingsCommand = serial.to_bytes([0x02, 0x03, 0x03, 0x03, 0x03, 0x03])
StreamCommand = serial.to_bytes([0x03, 0x03, 0x03, 0x03, 0x03, 0x03])

#Data = [b'\x08\x03\x7f\xff\xff\xff', b'\x08\n\x00\x01p\x97', b'\x08\x02\x00\x01.o', b'\x08\x02\x00\x00U\x99']

#print([comport.device for comport in serial.tools.list_ports.comports()])

with serial.Serial('/dev/ttyUSB0',115200, timeout = 3) as ser:

    ContinueCommand = serial.to_bytes([0x08,0x08, 0x08,0x08, 0x08,0x08])

    ser.write(StreamCommand)
    Data = []
    Iter = 0

    while True:

        for i in range(4):
            Data.append(ser.read(6))

        Continue = ser.read(1)

        if Continue == b'\x08':
            break
        else:
            ser.write(ContinueCommand)

    print(DecodeDataPacket(Data[0]))