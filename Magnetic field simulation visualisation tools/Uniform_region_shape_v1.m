clear;
clc;

filename = "Helmholtz_Coils_induction[100x100x100].csv";

File = fopen(filename,'rt');

bin = textscan(File,'%s',5,'Delimiter','\n');
DATA = textscan(File,'%f%f%f%f32%f32%f32%s','Delimiter',',');

fclose(File);

XPos = DATA(:,1);
YPos = DATA(:,2);
ZPos = DATA(:,3);
InductionX = DATA(:,4);
InductionY = DATA(:,5);
InductionZ = DATA(:,6);

MaxRange = single(size(InductionX{1},1)^(1/3) - 1);
HalfRange = MaxRange/2;
NumPoints = MaxRange + 1;

BoundryPercentage = 4;

for k = 1:1:size(InductionX{1},1)

    if XPos{1}(k) == 0 && YPos{1}(k) == 0 && ZPos{1}(k) == 0
        ZeroIndex = k;
        break
    end
end

StdDevX = std(InductionX{1});
StdDevY = std(InductionY{1});

InductionDeviationX = zeros(size(InductionX{1},1),1);
InductionDeviationY = zeros(size(InductionY{1},1),1);
InductionDeviationZ = zeros(size(InductionZ{1},1),1);
netPointDeviation = zeros(size(InductionY{1},1),1);

for k = 1:1:size(InductionX{1},1)

InductionDeviationX(k) = abs(1 - InductionX{1}(k)/(InductionX{1}(ZeroIndex)+1e-12)) * 100;
InductionDeviationY(k) = abs(1 - InductionY{1}(k)/(InductionY{1}(ZeroIndex)+1e-12)) * 100;
InductionDeviationZ(k) = abs(1 - InductionZ{1}(k)/(InductionZ{1}(ZeroIndex)+1e-12)) * 100;

netPointDeviation(k) = sqrt(InductionDeviationY(k)^2 + InductionDeviationX(k)^2 + InductionDeviationZ(k)^2);
%netPointDeviation(k) = abs(1 - InductionZ{1}(k)/InductionZ{1}(ZeroIndex));

end

Iterator = 1;

for k = 1:1:size(InductionX{1},1)

    if ZPos{1}(k) == 0
        
        NewX(Iterator) = XPos{1}(k);
        NewY(Iterator) = YPos{1}(k);
        NewZ(Iterator) = ZPos{1}(k);
        NewPointDeviation(Iterator) = netPointDeviation(k);
        
        Iterator = Iterator + 1;
        
    end
end

FinalMapData(:,1) = transpose(NewX); 
FinalMapData(:,2) = transpose(NewY); 
FinalMapData(:,3) = transpose(NewPointDeviation); 

Xvalues = FinalMapData(:,1) ; 
Yvalues = FinalMapData(:,2) ; 
DeviationValues = FinalMapData(:,3) ; 

NX = length(unique(Xvalues)) ; 
NY = length(unique(Yvalues)) ; 
Xx = reshape(Xvalues,NX,NY) ; 
Yy = reshape(Yvalues,NX,NY) ; 

Devs = reshape(netPointDeviation,NumPoints,NumPoints,NumPoints);

[xx,yy,zz] = meshgrid(-HalfRange:1:HalfRange);
RR = double((-HalfRange:1:HalfRange));

h = contourslice(double(xx),double(yy),double(zz),double(Devs),[],RR,[],[0,BoundryPercentage]);
set(h,'EdgeColor',[0 0.06 1],'LineWidth',1)

xlabel('Distance along X [cm]')
ylabel('Distance along Y [cm]')
zlabel('Distance along Z [cm]')
title({'Region with magnetic field induction deviation',['less or equal to ',num2str(BoundryPercentage),'% of the central value']})
set(gca,'XLim',[-50 50],'YLim',[-50 50],'ZLim',[-50 50])

hold on
view(-50,15)

[SX,SY,SZ] = sphere(30);

surf(16*SX,16*SY,16*SZ);
colormap([0.15 1 0.15])



