%{
Author:

Rafa� Osadnik

Silesian University of Technology
Institute of Physics

Program written for:

Helmholtz Coils Clinostat PBL POLSL

To keep up to date, visit our fanpage at: www.facebook.com/helmoltzpolsl/

------------------------------------------------------------------------------------------------------------------------

Supported by EU project: Projekt wdro�eniowy POWER 3.5 p.t. "Politechnika �l�ska jako Centrum Nowoczesnego Kszta�cenia
opartego o badania i innowacje" (POWR.03.05.00-IP.08-00-PZ1/17), finansowany z Funduszy Europejskich Programu Operacyjnego
Wiedza Edukacja Rozw�j (PO WER 3.5).

%}

clear;
clc;

filename = "Helmholtz_Coils_induction[60x60x60].csv";

File = fopen(filename,'rt');

bin = textscan(File,'%s',5,'Delimiter','\n');
DATA = textscan(File,'%f%f%f%f32%f32%f32%s','Delimiter',',');

fclose(File);

XPos = DATA(:,1);
YPos = DATA(:,2);
ZPos = DATA(:,3);
InductionX = DATA(:,4);
InductionY = DATA(:,5);
InductionZ = DATA(:,6);

for k = 1:1:size(InductionX{1},1)

    if XPos{1}(k) == 0 && YPos{1}(k) == 0 && ZPos{1}(k) == 0
        ZeroIndex = k;
        break
    end
end

StdDevX = std(InductionX{1});
StdDevY = std(InductionY{1});

InductionDeviationX = zeros(size(InductionX{1},1),1);
InductionDeviationY = zeros(size(InductionY{1},1),1);
InductionDeviationZ = zeros(size(InductionZ{1},1),1);
netPointDeviation = zeros(size(InductionY{1},1),1);

for k = 1:1:size(InductionX{1},1)

InductionDeviationX(k) = abs(1 - InductionX{1}(k)/(InductionX{1}(ZeroIndex)+1e-12)) * 100;
InductionDeviationY(k) = abs(1 - InductionY{1}(k)/(InductionY{1}(ZeroIndex)+1e-12)) * 100;
InductionDeviationZ(k) = abs(1 - InductionZ{1}(k)/(InductionZ{1}(ZeroIndex)+1e-12)) * 100;

netPointDeviation(k) = sqrt(InductionDeviationY(k)^2 + InductionDeviationX(k)^2 + InductionDeviationZ(k)^2);
%netPointDeviation(k) = abs(1 - InductionZ{1}(k)/InductionZ{1}(ZeroIndex));

end

for z = XPos{1}(1):10:XPos{1}(end)
    
    Iterator = 1;

for k = 1:1:size(InductionX{1},1)

    if ZPos{1}(k) == z
        
        NewX(Iterator) = XPos{1}(k);
        NewY(Iterator) = YPos{1}(k);
        NewZ(Iterator) = ZPos{1}(k);
        NewPointDeviation(Iterator) = netPointDeviation(k);
        
        Iterator = Iterator + 1;
        
    end
end

FinalMapData(:,1) = transpose(NewX); 
FinalMapData(:,2) = transpose(NewY); 
FinalMapData(:,3) = transpose(NewPointDeviation); 

Xvalues = FinalMapData(:,1) ; 
Yvalues = FinalMapData(:,2) ; 
DeviationValues = FinalMapData(:,3) ; 

NX = length(unique(Xvalues)) ; 
NY = length(unique(Yvalues)) ; 
Xx = reshape(Xvalues,NX,NY) ; 
Yy = reshape(Yvalues,NX,NY) ; 
Devv = reshape(DeviationValues,NX,NY) ; 
tr = hgtransform;
[~,hContour] = contourf(Xx,Yy, Devv,'Parent',tr,[0 1 2 3 4 5 6 7 8 9 10]);
ax = gca;

set(ax,'XGrid','on');

set(tr,'Matrix',makehgtform('xrotate',pi/2)*makehgtform('translate',0,0,z))

ax = gca;

set(ax,'XGrid','on');

hold on

end

set(gca,'ZLim',[XPos{1}(1) XPos{1}(end)]);
xlabel('Distance along X axis [cm]')
ylabel('Distance along Z axis [cm]')
zlabel('Distance along Y axis [cm]')
title("Deviation of magnetic field induction from the value in (0,0,0) at 10 [cm] Z distance increments. Deviation given in %s of central field value ")
colorbar;

set(gcf,'Position',[100 100 2000 500])

view(80,20)



