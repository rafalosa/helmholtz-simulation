import matplotlib.pyplot as plt
import numpy as np
from scipy import constants as const
import copy
import math

def RotateAlongX(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[1,0,0],[0,np.cos(angleRad),-np.sin(angleRad)],[0,np.sin(angleRad),np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OX

    return np.dot(RotationMatrix,vec)

def RotateAlongY(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),0,-np.sin(angleRad)],[0,1,0],[np.sin(angleRad),0 ,np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OY

    return np.dot(RotationMatrix,vec)

def RotateAlongZ(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),-np.sin(angleRad),0],[np.sin(angleRad),np.cos(angleRad),0],[0,0,1]]) #Defioniowanie macierzy obrotu o kąt, wokół OZ

    return np.dot(RotationMatrix,vec)

class HelmholtzCoils:  # Klasa generująca układ dwóch cewek Helmholtza o zadanych parametrach, kolejnymi argumentami
    # kostruktora są: płaszczyna w której leżą cewki ('XY','XZ','YZ'), kształt cewek, na chwilę obecną 'octagonal'
    # oraz 'square', długość boku wielokąta cewki, ilość zwojów, średnica drutu, odległość między cewkami

    def __init__(self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance):

        self.Turns = NumberOfTurns
        self.WireGauge = WireGauge
        self.Distance = round(Distance,2)  # Inicjalizacja składowych klasy
        self.SideLength = Side
        self.Plane = Plane
        self.Polygon = NumberOfSides
        self.RotationAngle = 360/NumberOfSides
        self.InternalAngle = (180 * NumberOfSides - 360)/NumberOfSides


    def CalculateInduction(self, Current, AxisOffset1, AxisOffset2):  # Proponuję dodać wybór osi tutaj jako argument, np liczba oznaczająca przesunięsie wybranej osi od osi zerowej

        global dl, ProfileWidth, SimCubeSide  # Krok dl aproksymacji numerycznej
        AxisStep = 0.01  # Krok na osi względem której obliczane jest pole
        SimReach = 1  # Zasięg symulacji, oznacza to, że obliczone zostanie pole w odległości 1 m, wokół każdej z cewek

        if self.Plane == 'XY' or self.Plane == 'YX':  # Definiowanie wektora przesuwającego punkt na osi, zadanie początkowego punktu na osi oraz na boku cewki
                AxisStepVector = np.array([0, 0, AxisStep])
                PointOnAxis = np.array([0, 0, float(-SimReach)])
                StartingPoint = np.array([self.SideLength / 2 - AxisOffset1/100, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi) - AxisOffset2/100, 0.])

        elif self.Plane == 'XZ' or self.Plane == 'ZX':
                AxisStepVector = np.array([0, AxisStep, 0])
                PointOnAxis = np.array([0, float(-SimReach), 0])
                StartingPoint = np.array([self.SideLength / 2 - AxisOffset1/100, 0., -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi) - AxisOffset2/100])

        elif self.Plane == 'YZ' or self.Plane == 'ZY':
                AxisStepVector = np.array([AxisStep, 0, 0])
                PointOnAxis = np.array([float(-SimReach), 0, 0])
                StartingPoint = np.array([0., self.SideLength / 2 - AxisOffset1/100, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi) - AxisOffset2/100])
        R = np.zeros(int(SimReach / AxisStep))
        B = np.zeros((int(SimReach / AxisStep),3))

        N = int(self.SideLength / dl)  # Ilość kroków na jeden bok

        for j in range(len(B)):

            NetInductionForPoint = np.zeros(3)

            for CurrentTurn in range(self.Turns):  # Pętla odpowiadająca za kolejne zwoje

                NetInductionForCoil = np.zeros(3)

                PresentPoint = copy.deepcopy(StartingPoint)  # Powrót do punktu początkowego
                #print(PresentPoint[1])
                #print(self.Plane)

                if self.Plane == 'XY' or self.Plane == 'YX':
                    ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi), 0]) * dl

                elif self.Plane == 'XZ' or self.Plane == 'ZX':
                    ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), 0, np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

                elif self.Plane == 'YZ' or self.Plane == 'ZY':
                    ApproxVector = np.array([0, np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

                for Side in range(self.Polygon):

                    for Step in range(N):
                        DistanceVector = PointOnAxis + PresentPoint + PointOnAxis / np.linalg.norm(PointOnAxis) * (CurrentTurn % math.floor(ProfileWidth/self.WireGauge)) * self.WireGauge  # Obliczanie wektora położenia dla punktu na cewce

                        NetInductionForCoil += BiotSavart(ApproxVector, DistanceVector,
                                                       Current)  # Dodawanie kolejnych przyrostów indukcji

                        PresentPoint += ApproxVector  # Przesunięcie punktu na obwodzie cewki

                    if self.Plane == 'XY' or self.Plane == 'YX':
                        ApproxVector = RotateAlongZ(ApproxVector, self.RotationAngle)

                    elif self.Plane == 'XZ' or self.Plane == 'ZX':
                        ApproxVector = RotateAlongY(ApproxVector, self.RotationAngle)

                    elif self.Plane == 'YZ' or self.Plane == 'ZY':
                        ApproxVector = RotateAlongX(ApproxVector, self.RotationAngle)

                NetInductionForPoint += NetInductionForCoil

            # if(j == len(B) -1):
            #     print(NetInductionForPoint[-1]/InductionFromSquareCoil(Current, 1, AxisOffset))

            if self.Plane == 'XY' or self.Plane == 'YX':
                R[j] = PointOnAxis[2]

            elif self.Plane == 'XZ' or self.Plane == 'ZX':
                R[j] = PointOnAxis[1]

            elif self.Plane == 'YZ' or self.Plane == 'ZY':
                R[j] = PointOnAxis[0]

            PointOnAxis += AxisStepVector

            B[j] = NetInductionForPoint

        RFlip = -R[1:]
        RCoil1 = np.concatenate((R, np.flip(RFlip))) - round(self.Distance / 2,2)
        BCoil1 = np.concatenate((B, np.flip(B[1:],0)))
        RCoil2 = RCoil1 + self.Distance
        BCoil2 = BCoil1

        for i in range(len(RCoil1)):

            if RCoil1[i] >= RCoil2[0]:
                '''
                print(RCoil1[i])
                print(RCoil2[0])
                print(len(RCoil1))
                print(len(RCoil2))
                '''
                LeftIndex = i
                break

        RBothCoils = np.arange(RCoil2[0], RCoil1[-1], AxisStep)
        BBothCoils = np.zeros((len(RBothCoils),3))

        for i in range(len(BBothCoils)):
            BBothCoils[i] = BCoil1[LeftIndex + i - 2] + BCoil2[i]

        [val,ind] = MiddleValue(RBothCoils)
        Ext = int(SimCubeSide)
        return RBothCoils[ind - Ext : ind + Ext + 1],BBothCoils[ind - Ext : ind + Ext + 1]

    def HomogeneityArea(self, Current, OffsetStep, OffsetMax):

        [R0, B0] = self.CalculateInduction(Current, 0, 0)

        B = np.zeros((int(2*OffsetMax/OffsetStep) + 1, int(2*OffsetMax/OffsetStep) + 1, len(B0), 3))


        B[int(OffsetMax), int(OffsetMax)] = B0

        for i in range(int(OffsetMax) + 1, int(2*OffsetMax) + 1, OffsetStep):
            for j in range(int(OffsetMax), i+1, OffsetStep):
                [ROffset, BOffset] = self.CalculateInduction(Current, i - int(OffsetMax), j - OffsetMax)
                #print(BOffset)
                B[i,j] = BOffset
                B[j,i] = BOffset
                B[i - int(OffsetMax),j] = BOffset
                B[j - int(OffsetMax),i] = BOffset
                B[i - int(OffsetMax),j - int(OffsetMax)] = BOffset
                B[j - int(OffsetMax),i - int(OffsetMax)] = BOffset
                B[i,j - int(OffsetMax)] = BOffset
                B[j,i - int(OffsetMax)] = BOffset
                #print(i,j)
        return B

def BiotSavart(DL, DistanceVector, Current):

    dB = const.mu_0 * Current / 4 / const.pi * np.cross(DL,DistanceVector) / np.linalg.norm(DistanceVector) ** 3
    # Formuła bez wersora, więc wartość wektora odległości musi być do potęgi trzeciej (tutaj był błąd)

    return dB

def InductionFromCircularCoil(Turns,Current,Radius):

    SimReach = 2*Radius
    X1 = np.arange(-SimReach,SimReach,0.01)
    B1 = np.zeros(len(X1))

    for i in range(len(X1)):
        B1[i] = const.mu_0 * Turns * Current * Radius**2 / 2 / (Radius**2 + X1[i]**2)**(3/2)

    X2 = -X1[1:]

    XC1 = np.concatenate((X1, np.flip(X2))) - Radius / 2
    BCC1 = np.concatenate((B1, np.flip(B1[1:])))

    XC2 = XC1 + Radius
    BCC2 = BCC1

    XC3 = np.arange(XC2[0], XC1[-1], 0.01)
    BCC3 = np.zeros(len(XC3))

    for i in range(len(XC1)):

        if XC1[i] >= XC2[0]:
            LeftIndex = i
            break

    for i in range(len(XC3)):
        BCC3[i] = BCC1[i + LeftIndex - 2] + BCC2[i]

    return XC3,BCC3

def InductionFromSquareCoil(I, a, R):
    cos_alfa = (0.5*a - R)/np.sqrt( (0.5*a)**2 + (0.5*a - R)**2)
    cos_beta = (0.5*a + R)/np.sqrt( (0.5*a)**2 + (0.5*a + R)**2)
    cos_gamma = (0.5*a)/np.sqrt( (0.5*a)**2 + (0.5*a + R)**2)
    cos_delta = (0.5*a)/np.sqrt( (0.5*a)**2 + (0.5*a - R)**2)

    return const.mu_0*I /4 /const.pi *(4*(cos_alfa +cos_beta)/a + 2*cos_delta/(0.5*a - R) + 2*cos_gamma/(0.5*a + R))

def MiddleValue(values):
    length = len(values)
    if (length%2):
        index = int(length/2 - 0.5)
        midValue = values[index]
    else:
        index = int(length/2)
        midValue = ( values[int(length/2)] +  values[int(length/2 - 1)])/2

    return midValue, index


dl = 0.01
Current = 1.3
ProfileWidth = 0.024
SimCubeSide = 10  # [cm]
CS1 = HelmholtzCoils('XY', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)
CS2 = HelmholtzCoils('YZ', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)
CS3 = HelmholtzCoils('XZ', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)

B1 = CS1.HomogeneityArea(Current, 1, SimCubeSide)
#B2 = CS2.HomogeneityArea(Current, 1, SimCubeSide)
#B3 = CS3.HomogeneityArea(Current, 1, SimCubeSide)

#[r0,B0] = CS2.CalculateInduction(1.3,0.1,0.1)
#B1 = CS1.CalculateInduction(1.3,0.1,0.1)






