import matplotlib.pyplot as plt
import numpy as np
from scipy import constants as const
import copy
import math

def RotateAlongX(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[1,0,0],[0,np.cos(angleRad),-np.sin(angleRad)],[0,np.sin(angleRad),np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OX

    return np.dot(RotationMatrix,vec)

def RotateAlongY(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),0,-np.sin(angleRad)],[0,1,0],[np.sin(angleRad),0 ,np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OY

    return np.dot(RotationMatrix,vec)

def RotateAlongZ(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),-np.sin(angleRad),0],[np.sin(angleRad),np.cos(angleRad),0],[0,0,1]]) #Defioniowanie macierzy obrotu o kąt, wokół OZ

    return np.dot(RotationMatrix,vec)

class HelmholtzCoils:  # Klasa generująca układ dwóch cewek Helmholtza o zadanych parametrach, kolejnymi argumentami
    # kostruktora są: płaszczyna w której leżą cewki ('XY','XZ','YZ'), kształt cewek, na chwilę obecną 'octagonal'
    # oraz 'square', długość boku wielokąta cewki, ilość zwojów, średnica drutu, odległość między cewkami

    def __init__(self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance):

        self.Turns = NumberOfTurns
        self.WireGauge = WireGauge
        self.Distance = round(Distance,2)  # Inicjalizacja składowych klasy
        self.SideLength = Side
        self.Plane = Plane
        self.Polygon = NumberOfSides
        self.RotationAngle = 360/NumberOfSides
        self.InternalAngle = (180 * NumberOfSides - 360)/NumberOfSides


        if self.Plane == 'XY' or self.Plane == 'YX':
            self.Index = 2
        elif self.Plane == 'XZ' or self.Plane == 'ZX':
            self.Index = 1
        elif self.Plane == 'YZ' or self.Plane == 'ZY':
            self.Index = 0

    def CalculateInduction(self, Current, x, y, z):  # Proponuję dodać wybór osi tutaj jako argument, np liczba oznaczająca przesunięsie wybranej osi od osi zerowej

        global dl, ProfileWidth, SimCubeSide  # Krok dl aproksymacji numerycznej

        if self.Plane == 'XY' or self.Plane == 'YX':  # Definiowanie wektora przesuwającego punkt na osi, zadanie początkowego punktu na osi oraz na boku cewki

                PointOnAxis = np.array([0, 0, -z/100])
                StartingPoint = np.array([self.SideLength / 2 - x/100, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi) - y/100, -z/100])

        elif self.Plane == 'XZ' or self.Plane == 'ZX':

                PointOnAxis = np.array([0, -y/100, 0])
                StartingPoint = np.array([self.SideLength / 2 - x/100, -y/100, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi) - z/100])

        elif self.Plane == 'YZ' or self.Plane == 'ZY':

                PointOnAxis = np.array([-x/100, 0, 0])
                StartingPoint = np.array([-x/100, self.SideLength / 2 - y/100, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi) - z/100])

        B = np.zeros(3)

        N = int(self.SideLength / dl)  # Ilość kroków na jeden bok

        NetInductionForPoint = np.zeros(3)

        for CurrentTurn in range(self.Turns):  # Pętla odpowiadająca za kolejne zwoje

            NetInductionForCoil = np.zeros(3)

            PresentPoint = copy.deepcopy(StartingPoint)  # Powrót do punktu początkowego
                #print(PresentPoint[1])
                #print(self.Plane)

            if self.Plane == 'XY' or self.Plane == 'YX':
                ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi), 0]) * dl

            elif self.Plane == 'XZ' or self.Plane == 'ZX':
                ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), 0, np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

            elif self.Plane == 'YZ' or self.Plane == 'ZY':
                ApproxVector = np.array([0, np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

            UnitarVector = np.zeros(3)
            UnitarVector[self.Index] = 1

            for Side in range(self.Polygon):

                for Step in range(N):
                    DistanceVector = PointOnAxis + PresentPoint + UnitarVector * (CurrentTurn % math.floor(ProfileWidth/self.WireGauge)) * self.WireGauge  # Obliczanie wektora położenia dla punktu na cewce

                    NetInductionForCoil += BiotSavart(ApproxVector, DistanceVector, Current)  # Dodawanie kolejnych przyrostów indukcji

                    PresentPoint += ApproxVector  # Przesunięcie punktu na obwodzie cewki

                if self.Plane == 'XY' or self.Plane == 'YX':
                    ApproxVector = RotateAlongZ(ApproxVector, self.RotationAngle)

                elif self.Plane == 'XZ' or self.Plane == 'ZX':
                    ApproxVector = RotateAlongY(ApproxVector, self.RotationAngle)

                elif self.Plane == 'YZ' or self.Plane == 'ZY':
                    ApproxVector = RotateAlongX(ApproxVector, self.RotationAngle)

            NetInductionForPoint += NetInductionForCoil

        B = NetInductionForPoint


        return B

    def HomogeneityArea(self, Current, OffsetStep, OffsetMax):


        FirstQuarterB = np.zeros((int(OffsetMax/OffsetStep) + 1, int(OffsetMax/OffsetStep) + 1, 2*SimCubeSide + 1, 3))
        SecondQuarterB = np.zeros((int(OffsetMax/OffsetStep), int(OffsetMax/OffsetStep) + 1, 2*SimCubeSide + 1, 3))


        for i in range(len(FirstQuarterB)):
            for j in range(i+1):
                [R, B] = self.CalculateInduction(Current, i, j)
                B[:,self.Index - 2] = -B[:,self.Index - 2]
                B[:,self.Index - 1] = -B[:,self.Index - 1]
                FirstQuarterB[i,j] = B
                FirstQuarterB[j,i] = B
                print(i,j)

        for i in range(len(SecondQuarterB)):
            for j in range(len(SecondQuarterB) + 1):
                SecondQuarterB[i,j] = FirstQuarterB[-i-1,j]

        print(FirstQuarterB)
        print(SecondQuarterB)

        return B

def BiotSavart(DL, DistanceVector, Current):

    dB = const.mu_0 * Current / 4 / const.pi * np.cross(DL,DistanceVector) / np.linalg.norm(DistanceVector) ** 3
    # Formuła bez wersora, więc wartość wektora odległości musi być do potęgi trzeciej (tutaj był błąd)

    return dB

def InductionFromCircularCoil(Turns,Current,Radius):

    SimReach = 2*Radius
    X1 = np.arange(-SimReach,SimReach,0.01)
    B1 = np.zeros(len(X1))

    for i in range(len(X1)):
        B1[i] = const.mu_0 * Turns * Current * Radius**2 / 2 / (Radius**2 + X1[i]**2)**(3/2)

    X2 = -X1[1:]

    XC1 = np.concatenate((X1, np.flip(X2))) - Radius / 2
    BCC1 = np.concatenate((B1, np.flip(B1[1:])))

    XC2 = XC1 + Radius
    BCC2 = BCC1

    XC3 = np.arange(XC2[0], XC1[-1], 0.01)
    BCC3 = np.zeros(len(XC3))

    for i in range(len(XC1)):

        if XC1[i] >= XC2[0]:
            LeftIndex = i
            break

    for i in range(len(XC3)):
        BCC3[i] = BCC1[i + LeftIndex - 2] + BCC2[i]

    return XC3,BCC3

def InductionFromSquareCoil(I, a, R):
    cos_alfa = (0.5*a - R)/np.sqrt( (0.5*a)**2 + (0.5*a - R)**2)
    cos_beta = (0.5*a + R)/np.sqrt( (0.5*a)**2 + (0.5*a + R)**2)
    cos_gamma = (0.5*a)/np.sqrt( (0.5*a)**2 + (0.5*a + R)**2)
    cos_delta = (0.5*a)/np.sqrt( (0.5*a)**2 + (0.5*a - R)**2)

    return const.mu_0*I /4 /const.pi *(4*(cos_alfa +cos_beta)/a + 2*cos_delta/(0.5*a - R) + 2*cos_gamma/(0.5*a + R))

def MiddleValue(values):
    length = len(values)
    if (length%2):
        index = int(length/2 - 0.5)
        midValue = values[index]
    else:
        index = int(length/2)
        midValue = ( values[int(length/2)] +  values[int(length/2 - 1)])/2

    return midValue, index


dl = 0.01
Current = 1.3
ProfileWidth = 0.024
SimCubeSide = 2  # [cm]
CS1 = HelmholtzCoils('XY', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)
CS2 = HelmholtzCoils('YZ', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)
CS3 = HelmholtzCoils('XZ', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)

for i in range(-10,10,1):
    #if i != 0:
    B1 = CS1.CalculateInduction(Current, 0, 0, i)
    print(B1)
    plt.plot(i,B1[2],'or')

plt.show()

#B2 = CS2.HomogeneityArea(Current, 1, SimCubeSide)
#B3 = CS3.HomogeneityArea(Current, 1, SimCubeSide)

NetInduction = np.zeros((SimCubeSide,SimCubeSide,SimCubeSide,3))
