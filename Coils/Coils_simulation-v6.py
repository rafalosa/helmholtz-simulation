import matplotlib.pyplot as plt
import numpy as np
from scipy import constants as const
import copy
import math

def RotateAlongX(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[1,0,0],[0,np.cos(angleRad),-np.sin(angleRad)],[0,np.sin(angleRad),np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OX

    return np.dot(RotationMatrix,vec)


def RotateAlongY(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),0,-np.sin(angleRad)],[0,1,0],[np.sin(angleRad),0 ,np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OY

    return np.dot(RotationMatrix,vec)


def RotateAlongZ(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),-np.sin(angleRad),0],[np.sin(angleRad),np.cos(angleRad),0],[0,0,1]]) #Defioniowanie macierzy obrotu o kąt, wokół OZ

    return np.dot(RotationMatrix,vec)


class HelmholtzCoils:  # Klasa generująca układ dwóch cewek Helmholtza o zadanych parametrach, kolejnymi argumentami
    # kostruktora są: płaszczyna w której leżą cewki ('XY','XZ','YZ'), kształt cewek, na chwilę obecną 'octagonal'
    # oraz 'square', długość boku wielokąta cewki, ilość zwojów, średnica drutu, odległość między cewkami

    def __init__(self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance):

        self.Turns = NumberOfTurns
        self.WireGauge = WireGauge
        self.Distance = round(Distance,2)  # Inicjalizacja składowych klasy
        self.SideLength = Side
        self.Plane = Plane
        self.Polygone = NumberOfSides
        self.RotationAngle = 360/NumberOfSides
        self.InternalAngle = (180 * NumberOfSides - 360)/NumberOfSides

    def CalculateInduction(self, Current, AxisOffset):

        global dl, ProfileWidth  # Krok dl aproksymacji numerycznej
        AxisStep = 0.01  # Krok na osi względem której obliczane jest pole
        SimReach = 2 * self.Distance  # Zasięg symulacji, oznacza to, że obliczone zostanie pole w odległości 2*dystans między cewkami, wokół każdej z cewek

        if self.Plane == 'XY' or 'YX':  # Definiowanie wektora przesuwającego punkt na osi, zadanie początkowego punktu na osi oraz na boku cewki
                AxisStepVector = np.array([0, 0, AxisStep])
                PointOnAxis = np.array([AxisOffset, 0, float(-SimReach)])
                StartingPoint = np.array(
                    [self.SideLength / 2, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi), 0.])

        elif self.Plane == 'XZ' or 'ZX':
                AxisStepVector = np.array([0, AxisStep, 0])
                PointOnAxis = np.array([AxisOffset, float(-SimReach), 0])
                StartingPoint = np.array(
                    [self.SideLength / 2, 0., -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi)])

        elif self.Plane == 'YZ' or 'ZY':
                AxisStepVector = np.array([AxisStep, 0, 0])
                PointOnAxis = np.array([float(-SimReach), AxisOffset, 0])
                StartingPoint = np.array(
                    [0., self.SideLength / 2, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi)])

        R = np.zeros(int(SimReach / AxisStep))
        B = np.zeros(int(SimReach / AxisStep))

        N = int(self.SideLength / dl)  # Ilość kroków na jeden bok

        ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi), 0]) * dl  # Definiowanie wektora 'wodzącego' punkt po cewce

        for j in range(len(B)):

            NetInductionForPoint = np.zeros(3)

            for CurrentTurn in range(self.Turns):  # Pętla odpowiadająca za kolejne zwoje

                PresentPoint = copy.deepcopy(StartingPoint)  # Powrót do punktu początkowego
                InductionForSide = np.zeros(3)


                for Step in range(N):
                    DistanceVector = PointOnAxis + PresentPoint + PointOnAxis / np.linalg.norm(PointOnAxis) * (CurrentTurn % math.floor(ProfileWidth/self.WireGauge)) * self.WireGauge  # Obliczanie wektora położenia dla punktu na cewce

                    InductionForSide += BiotSavart(ApproxVector, DistanceVector,
                                                   Current)  # Dodawanie kolejnych przyrostów indukcji

                    PresentPoint += ApproxVector  # Przesunięcie punktu na obwodzie cewki

                NetInductionForCoil = np.zeros(3)
                IndRot = InductionForSide

                for Side in range(self.Polygone):

                    NetInductionForCoil += IndRot

                    if self.Plane == 'XY' or 'YX':

                        IndRot = RotateAlongZ(IndRot, self.RotationAngle)

                    elif self.Plane == 'XZ' or 'ZX':

                        IndRot = RotateAlongY(IndRot, self.RotationAngle)

                    elif self.Plane == 'YZ' or 'ZY':

                        IndRot = RotateAlongX(IndRot, self.RotationAngle)

                NetInductionForPoint += NetInductionForCoil
            #print(NetInductionForCoil[2])

            if self.Plane == 'XY' or 'YX':
                R[j] = PointOnAxis[2]

            elif self.Plane == 'XZ' or 'ZX':
                R[j] = PointOnAxis[1]

            elif self.Plane == 'YZ' or 'ZY':
                R[j] = PointOnAxis[0]

            PointOnAxis += AxisStepVector

            if self.Plane == 'XY' or 'YX':
                B[j] = np.abs(NetInductionForPoint[2])

            elif self.Plane == 'XZ' or 'ZX':
                B[j] = np.abs(NetInductionForPoint[1])

            elif self.Plane == 'ZY' or 'YZ':
                B[j] = np.abs(NetInductionForPoint[0])

        RFlip = -R[1:]
        RCoil1 = np.concatenate((R, np.flip(RFlip))) - round(self.Distance / 2,2)
        BCoil1 = np.concatenate((B, np.flip(B[1:])))
        RCoil2 = RCoil1 + self.Distance
        BCoil2 = BCoil1

        for i in range(len(RCoil1)):

            if RCoil1[i] >= RCoil2[0]:
                '''
                print(RCoil1[i])
                print(RCoil2[0])
                print(len(RCoil1))
                print(len(RCoil2))
                '''
                LeftIndex = i
                break

        RBothCoils = np.arange(RCoil2[0], RCoil1[-1], AxisStep)
        BBothCoils = np.zeros(len(RBothCoils))

        for i in range(len(BBothCoils)):
            BBothCoils[i] = BCoil1[LeftIndex + i - 2] + BCoil2[i]

        return RBothCoils,BBothCoils

    def HomogeneityArea(self, Current, ComplianceFactor, OffsetStep):

        [R0, B0] = self.CalculateInduction(Current, 0)
        [midValue, index] = MiddleValue(B0)
        print(midValue)

        for i in range(index, int(len(B0))):
            if not ((1. - ComplianceFactor)*midValue <= B0[i] <= (1. + ComplianceFactor)*midValue):
                perpendicularComponent = R0[i]
                break

        offset = OffsetStep
        [ROffset, BOffset] = self.CalculateInduction(Current, offset)
        [midValueOffset, indexOffset] = MiddleValue(BOffset)

        while (1. - ComplianceFactor)*midValue <= midValueOffset <= (1. + ComplianceFactor)*midValue:
            offset += OffsetStep
            [ROffset, BOffset] = self.CalculateInduction(Current, offset)
            [midValueOffset, indexOffset] = MiddleValue(BOffset)
            print(midValueOffset)

        parallelComponent = offset

        bounds = np.zeros((3,2))

        if self.Plane == 'XY' or 'YX':
            bounds[2,0] = -perpendicularComponent
            bounds[2,1] = perpendicularComponent
            bounds[1,0] = -parallelComponent
            bounds[1,1] = parallelComponent
            bounds[0,0] = -parallelComponent
            bounds[0,1] = parallelComponent

        elif self.Plane == 'XZ' or 'ZX':
            bounds[2,0] = -parallelComponent
            bounds[2,1] = parallelComponent
            bounds[1,0] = -perpendicularComponent
            bounds[1,1] = perpendicularComponent
            bounds[0,0] = -parallelComponent
            bounds[0,1] = parallelComponent

        elif self.Plane == 'YZ' or 'ZY':
            bounds[2,0] = -parallelComponent
            bounds[2,1] = parallelComponent
            bounds[1,0] = -parallelComponent
            bounds[1,1] = parallelComponent
            bounds[0,0] = -perpendicularComponent
            bounds[0,1] = perpendicularComponent


        return bounds,[R0, B0], [ROffset, BOffset]


def BiotSavart(DL, DistanceVector, Current):

    dB = const.mu_0 * Current / 4 / const.pi * np.cross(DL,DistanceVector) / np.linalg.norm(DistanceVector) ** 3
    # Formuła bez wersora, więc wartość wektora odległości musi być do potęgi trzeciej (tutaj był błąd)

    return dB


def MiddleValue(values):
    length = len(values)
    if (length%2):
        index = int(length/2 - 0.5)
        midValue = values[index]
    else:
        index = int(length/2)
        midValue = ( values[int(length/2)] +  values[int(length/2 - 1)])/2

    return midValue, index


def InductionFromCircularCoil(Turns,Current,Radius):

    SimReach = 2*Radius
    X1 = np.arange(-SimReach,SimReach,0.01)
    B1 = np.zeros(len(X1))

    for i in range(len(X1)):
        B1[i] = const.mu_0 * Turns * Current * Radius**2 / 2 / (Radius**2 + X1[i]**2)**(3/2)

    X2 = -X1[1:]

    XC1 = np.concatenate((X1, np.flip(X2))) - CoilRad / 2
    BCC1 = np.concatenate((B1, np.flip(B1[1:])))

    XC2 = XC1 + CoilRad
    BCC2 = BCC1

    XC3 = np.arange(XC2[0], XC1[-1], 0.01)
    BCC3 = np.zeros(len(XC3))

    for i in range(len(XC1)):

        if XC1[i] >= XC2[0]:
            LeftIndex = i
            break

    for i in range(len(XC3)):
        BCC3[i] = BCC1[i + LeftIndex - 2] + BCC2[i]

    return XC3,BCC3


dl = 0.01
Current = 1.3
CoilRad = 1
ProfileWidth = 0.024
Coils = []

CS = HelmholtzCoils('XY',8, 0.765367, 90, 0.0012, 0.94698)

#[bounds,[ROct, BOct],[ROctOffset, BOctOffset]] = CS.HomogeneityArea(Current, 0.1, 0.005)

# Coils.append(HelmholtzCoils('XY', 3, 2 * CoilRad * np.sin(const.pi / 3), 1, 0.0012, CoilRad * 2 * 0.283))
# Coils.append(HelmholtzCoils('XY', 4, 2 * CoilRad * np.sin(const.pi / 4), 1, 0.0012, CoilRad * 2 * 0.38502))
# Coils.append(HelmholtzCoils('XY', 5, 2 * CoilRad * np.sin(const.pi / 5), 1, 0.0012, CoilRad * 2 * 0.42904))
# Coils.append(HelmholtzCoils('XY', 6, 2 * CoilRad * np.sin(const.pi / 6), 1, 0.0012, CoilRad * 2 * 0.45178))
# Coils.append(HelmholtzCoils('XY', 8, 2 * CoilRad * np.sin(const.pi / 8), 90, 0.0012, CoilRad * 2 * 0.47349))

[RC,BC] = CS.CalculateInduction(Current, 0.02)

fig, ax = plt.subplots()

ax.plot(RC,BC)
print((0.00010930403838727145 - MiddleValue(BC)[0])/0.00010930403838727145)

plt.show()

'''
fig, ax = plt.subplots()

ax.plot(RRound, BRound, label='Okręgi')

for i in range(len(Coils)):
   [RC,BC] = Coils[i].CalculateInduction(Current,0)
    ax.plot(RC,BC,label='N={}'.format(Coils[i].Polygone))

ax.legend()
ax.set_ylabel('Indukcja pola magnetycznego [T]')
ax.set_xlabel('Dystans od punktu między cewkami [m]')
plt.show()

'''