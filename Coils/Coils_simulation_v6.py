import matplotlib.pyplot as plt
import numpy as np
from scipy import constants as const
import statistics
import copy

def RotateAlongX(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[1,0,0],[0,np.cos(angleRad),-np.sin(angleRad)],[0,np.sin(angleRad),np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OX

    return np.dot(RotationMatrix,vec)


def RotateAlongY(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),0,-np.sin(angleRad)],[0,1,0],[np.sin(angleRad),0 ,np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OY

    return np.dot(RotationMatrix,vec)


def RotateAlongZ(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),-np.sin(angleRad),0],[np.sin(angleRad),np.cos(angleRad),0],[0,0,1]]) #Defioniowanie macierzy obrotu o kąt, wokół OZ

    return np.dot(RotationMatrix,vec)

def MiddleValue(values):
    length = len(values)
    if (length%2):
        index = int(length/2 - 0.5)
        midValue = values[index]
    else:
        index = int(length/2)
        midValue = ( values[int(length/2)] +  values[int(length/2 - 1)])/2

    return midValue, index

class HelmholtzCoils:  # Klasa generująca układ dwóch cewek Helmholtza o zadanych parametrach, kolejnymi argumentami
    # kostruktora są: płaszczyna w której leżą cewki ('XY','XZ','YZ'), kształt cewek, na chwilę obecną 'octagonal'
    # oraz 'square', długość boku wielokąta cewki, ilość zwojów, średnica drutu, odległość między cewkami

    def __init__(self, Plane, Shape, Side, NumberOfTurns, WireGauge, Distance):

        self.Turns = NumberOfTurns
        self.WireGauge = WireGauge
        self.Distance = Distance  # Inicjalizacja składowych klasy
        self.SideLength = Side
        self.Plane = Plane
        self.Shape = Shape

        if Shape == 'octagonal':

            a1 = np.cos(45 / 360 * 2 * const.pi)
            a2 = np.sin(45 / 360 * 2 * const.pi)

            if Plane == 'XY' or'YX':

                VECTORS = np.zeros((8, 3))

                VECTORS[0] = np.array([a1, a2, 0])
                VECTORS[1] = np.array([0, 1, 0])
                VECTORS[2] = np.array([-a1, a2, 0])
                VECTORS[3] = np.array([-1, 0, 0])  # Kreacja kolejnych wektorów jednostkowych, które będą odpowiadały
                # za 'obrót' wektora wskazującego na kolejne fragmenty dl cewki

                VECTORS[4] = np.array([-a1, -a2, 0])
                VECTORS[5] = np.array([0, -1, 0])
                VECTORS[6] = np.array([a1, -a2, 0])
                VECTORS[7] = np.array([1, 0, 0])

                self.UnitVectors = VECTORS

            elif Plane == 'XZ' or 'ZX':

                VECTORS = np.zeros((8, 3))

                VECTORS[0] = np.array([a1, 0, a2])
                VECTORS[1] = np.array([0, 0, 1])
                VECTORS[2] = np.array([-a1, 0, a2])
                VECTORS[3] = np.array([-1, 0, 0])
                VECTORS[4] = np.array([-a1, 0, -a2])
                VECTORS[5] = np.array([0, 0, -1])
                VECTORS[6] = np.array([a1, 0, -a2])
                VECTORS[7] = np.array([1, 0, 0])

                self.UnitVectors = VECTORS

            elif Plane == 'YZ' or 'ZY':

                VECTORS = np.zeros((8, 3))

                VECTORS[0] = np.array([0, a1, a2])
                VECTORS[1] = np.array([0, 0, 1])
                VECTORS[2] = np.array([0, -a1, a2])
                VECTORS[3] = np.array([0, -1, 0])
                VECTORS[4] = np.array([0, -a1, -a2])
                VECTORS[5] = np.array([0, 0, -1])
                VECTORS[6] = np.array([0, a1, -a2])
                VECTORS[7] = np.array([0, 1, 0])

                self.UnitVectors = VECTORS

        elif Shape == 'square':

            if Plane == 'XY' or 'YX':

                VECTORS = np.zeros((4, 3))

                VECTORS[0] = [0, 1, 0]
                VECTORS[1] = [-1, 0, 0]
                VECTORS[2] = [0, -1, 0]
                VECTORS[3] = [1, 0, 0]

                self.UnitVectors = VECTORS

            elif Plane == 'XZ' or 'ZX':

                VECTORS = np.zeros((4, 3))

                VECTORS[0] = [0, 0, 1]
                VECTORS[1] = [-1, 0, 0]
                VECTORS[2] = [0, 0, -1]
                VECTORS[3] = [1, 0, 0]

                self.UnitVectors = VECTORS

            elif Plane == 'YZ' or 'ZY':

                VECTORS = np.zeros((4, 3))

                VECTORS[0] = [0, 0, 1]
                VECTORS[1] = [-1, 0, 0]
                VECTORS[2] = [0, 0, -1]
                VECTORS[3] = [1, 0, 0]

                self.UnitVectors = VECTORS

    def CalculateInduction(self, Current, AxisOffset):  # Proponuję dodać wybór osi tutaj jako argument, np liczba oznaczająca przesunięsie wybranej osi od osi zerowej

        global dl  # Krok dl aproksymacji numerycznej
        AxisStep = 0.01  # Krok na osi względem której obliczane jest pole
        SimReach = 4 * self.Distance  # Zasięg symulacji, oznacza to, że obliczone zostanie pole w odległości 2*dystans między cewkami, wokół każdej z cewek

        if self.Shape == 'octagonal':

            if self.Plane == 'XY' or 'YX':  # Definiowanie wektora przesuwającego punkt na osi, zadanie początkowego punktu na osi oraz na boku cewki
                AxisStepVector = np.array([0, 0, AxisStep])
                PointOnAxis = np.array([AxisOffset, 0, float(-SimReach)])
                StartingPoint = np.array(
                    [self.SideLength / 2, -self.SideLength/2 * np.tan(67.5 / 360 * 2 * const.pi), 0.])

            elif self.Plane == 'XZ' or 'ZX':
                AxisStepVector = np.array([0, AxisStep, 0])
                PointOnAxis = np.array([AxisOffset, float(-SimReach), 0])
                StartingPoint = np.array(
                    [self.SideLength / 2, 0., -self.SideLength/2 * np.tan(67.5 / 360 * 2 * const.pi)])

            elif self.Plane == 'YZ' or 'ZY':
                AxisStepVector = np.array([AxisStep, 0, 0])
                PointOnAxis = np.array([float(-SimReach), AxisOffset, 0])
                StartingPoint = np.array(
                    [0., self.SideLength / 2, -self.SideLength/2 * np.tan(67.5 / 360 * 2 * const.pi)])

        elif self.Shape == 'square':

            if self.Plane == 'XY' or 'YX':

                AxisStepVector = np.array([0, 0, AxisStep])
                PointOnAxis = np.array([AxisOffset, 0, float(-SimReach)])
                StartingPoint = np.array([self.SideLength / 2, -self.SideLength / 2, 0.])

            elif self.Plane == 'XZ' or 'ZX':

                AxisStepVector = np.array([0, AxisStep, 0])
                PointOnAxis = np.array([AxisOffset, float(-SimReach), 0])
                StartingPoint = np.array([self.SideLength / 2, 0., -self.SideLength / 2])

            elif self.Plane == 'YZ' or 'ZY':

                AxisStepVector = np.array([AxisStep, 0, 0])
                PointOnAxis = np.array([float(-SimReach), AxisOffset, 0])
                StartingPoint = np.array([0., self.SideLength / 2, -self.SideLength / 2])

        R = np.zeros(int(SimReach / AxisStep))
        B = np.zeros(int(SimReach / AxisStep))

        N = int(self.SideLength / dl)  # Ilość kroków na jeden bok

        ApproxVector = copy.deepcopy(self.UnitVectors[0]) * dl  # Definiowanie wektora 'wodzącego' punkt po cewce

        for j in range(len(B)):

            NetInductionForPoint = np.zeros(3)

            for CurrentTurn in range(self.Turns):  # Pętla odpowiadająca za kolejne zwoje

                PresentPoint = copy.deepcopy(StartingPoint)  # Powrót do punktu początkowego
                InductionForSide = np.zeros(3)

                for Step in range(N):
                    DistanceVector = (PointOnAxis - PresentPoint) + PointOnAxis / np.linalg.norm(
                        PointOnAxis) * self.WireGauge * CurrentTurn  # Obliczanie wektora położenia dla punktu na cewce

                    InductionForSide += BiotSavart(ApproxVector, DistanceVector,
                                                   Current)  # Dodawanie kolejnych przyrostów indukcji


                    PresentPoint += ApproxVector  # Przesunięcie punktu na obwodzie cewki

                NetInductionForCoil = np.zeros(3)
                IndRot = InductionForSide

                for Side in range(len(self.UnitVectors)):

                    NetInductionForCoil += IndRot

                    if self.Plane == 'XY' or 'YX':

                        IndRot = RotateAlongZ(IndRot, 360 / len(self.UnitVectors))

                    elif self.Plane == 'XZ' or 'ZX':

                        IndRot = RotateAlongY(IndRot, 360 / len(self.UnitVectors))

                    elif self.Plane == 'YZ' or 'ZY':

                        IndRot = RotateAlongX(IndRot,360 / len(self.UnitVectors))

                NetInductionForPoint += NetInductionForCoil

            if self.Plane == 'XY' or 'YX':
                R[j] = PointOnAxis[2]

            elif self.Plane == 'XZ' or 'ZX':
                R[j] = PointOnAxis[1]

            elif self.Plane == 'YZ' or 'ZY':
                R[j] = PointOnAxis[0]

            PointOnAxis += AxisStepVector

            if self.Plane == 'XY' or 'YX':
                B[j] = np.abs(NetInductionForPoint[2])

            elif self.Plane == 'XZ' or 'ZX':
                B[j] = np.abs(NetInductionForPoint[1])

            elif self.Plane == 'ZY' or 'YZ':
                B[j] = np.abs(NetInductionForPoint[0])

        RFlip = -R
        RCoil1 = np.concatenate((R, np.flip(RFlip))) - self.Distance/2
        BCoil1 = np.concatenate((B, np.flip(B)))
        RCoil2 = RCoil1 + self.Distance
        BCoil2 = BCoil1
        RBothCoils = np.arange(RCoil2[0], RCoil1[-1], AxisStep)
        BBothCoils = np.zeros(len(RBothCoils))

        for i in range(len(RCoil1)):

            if RCoil1[i] >= RCoil2[0]:
                LeftIndex = i
                break

        for i in range(len(RBothCoils)):
            BBothCoils[i] = BCoil1[i + LeftIndex - 3] + BCoil2[i]

        return RBothCoils,BBothCoils

    def HomogeneityArea(self, Current, ComplianceFactor, OffsetStep):

        [R0, B0] = self.CalculateInduction(Current, 0)
        [midValue, index] = MiddleValue(B0)

        for i in range(index, int(len(B0))):
            if not ((1. - ComplianceFactor)*midValue <= B0[i] <= (1. + ComplianceFactor)*midValue):
                perpendicularComponent = R0[i]
                break

        offset = OffsetStep
        [ROffset, BOffset] = self.CalculateInduction(Current, offset)
        [midValueOffset, indexOffset] = MiddleValue(BOffset)

        while (1. - ComplianceFactor)*midValue <= midValueOffset <= (1. + ComplianceFactor)*midValue:
            offset += OffsetStep
            [ROffset, BOffset] = self.CalculateInduction(Current, offset)
            [midValueOffset, indexOffset] = MiddleValue(BOffset)

        parallelComponent = offset

        bounds = np.zeros((3,2))

        if self.Plane == 'XY' or 'YX':
            bounds[2,0] = -perpendicularComponent
            bounds[2,1] = perpendicularComponent
            bounds[1,0] = -parallelComponent
            bounds[1,1] = parallelComponent
            bounds[0,0] = -parallelComponent
            bounds[0,1] = parallelComponent

        elif self.Plane == 'XZ' or 'ZX':
            bounds[2,0] = -parallelComponent
            bounds[2,1] = parallelComponent
            bounds[1,0] = -perpendicularComponent
            bounds[1,1] = perpendicularComponent
            bounds[0,0] = -parallelComponent
            bounds[0,1] = parallelComponent

        elif self.Plane == 'YZ' or 'ZY':
            bounds[2,0] = -parallelComponent
            bounds[2,1] = parallelComponent
            bounds[1,0] = -parallelComponent
            bounds[1,1] = parallelComponent
            bounds[0,0] = -perpendicularComponent
            bounds[0,1] = perpendicularComponent


        return bounds,[R0, B0], [ROffset, BOffset]

def BiotSavart(DL, DistanceVector, Current):

    dB = const.mu_0 * Current / 4 / const.pi * np.cross(DL,DistanceVector) / np.linalg.norm(DistanceVector) ** 3
    # Formuła bez wersora, więc wartość wektora odległości musi być do potęgi trzeciej (tutaj był błąd)

    return dB

def InductionFromCircularCoil(Turns,Current,Radius):

    SimReach = 4*Radius
    X1 = np.arange(-SimReach,SimReach,0.01)
    B1 = np.zeros(len(X1))

    for i in range(len(X1)):
        B1[i] = const.mu_0 * Turns * Current * Radius**2 / 2 / (Radius**2 + X1[i]**2)**(3/2)

    X2 = -X1

    XC1 = np.concatenate((X1, np.flip(X2))) - CoilRad / 2
    BCC1 = np.concatenate((B1, np.flip(B1)))

    XC2 = XC1 + CoilRad
    BCC2 = BCC1

    XC3 = np.arange(XC2[0], XC1[-1], 0.01)
    BCC3 = np.zeros(len(XC3))

    for i in range(len(XC1)):

        if XC1[i] >= XC2[0]:
            LeftIndex = i
            break

    for i in range(len(XC3)):
        BCC3[i] = BCC1[i + LeftIndex - 3] + BCC2[i]

    return XC3,BCC3


dl = 0.01
Current = 1.3
OctSide = 1.
CoilRad = np.sqrt(1 + np.sqrt(2)*OctSide/2)
#CoilRad = OctSide/2 * np.sqrt(2)

CS = HelmholtzCoils('XY', 'octagonal', OctSide, 10, 0.0012, CoilRad)

#[ROct, BOct] = CS.CalculateInduction(Current, 0)

[bounds,[ROct, BOct],[ROctOffset, BOctOffset]] = CS.HomogeneityArea(Current, 0.01, 0.01)

print(bounds)

[RRound,BRound] = InductionFromCircularCoil(10,Current,CoilRad)

plt.plot(ROct, BOct)
plt.plot(RRound, BRound)

plt.show()
