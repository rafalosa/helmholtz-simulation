import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from scipy import constants as const
import copy
import multiprocessing as mp
from multiprocessing import Pool
from functools import partial
import math
import csv

'''
def HelmholtzSuperposition(Coils, Currents, side): #Coils - lista obiektów HelmholtzCoils, Currents - array/lista
    # natężeń prądów, side - ilość punktów na które dzielony jest bok badanego obszaru - side = 5 -> badany jest
    # sześcian o boku 4 cm

    B = []
    Mid = int(side / 2)

    for i in range(len(Coils)):

        B.append(np.zeros((side,side,side,3)))

    if side % 2 == 1:

        Progress = 0
        for i in range(Mid+1):
            for j in range(Mid+1):
                for k in range(Mid+1):
                    for z in range(len(Coils)):
                        B[z][i,j,k] = Coils[z].CalculateInduction(Currents[z],i - Mid, j - Mid, k - Mid)

                    Progress += 1
                    if not (Progress % 10):
                        print('Progres: {:.2f}%'.format(Progress / (Mid+1)**3 * 100))
        print('Progres: {}%'.format(100))

        for i in range(len(Coils)):
            BQuarter = B[i][:Mid,:Mid,:Mid]

            if Coils[i].Plane == 'XY' or Coils[i].Plane == 'YX':

                B[i][Mid+1:,:Mid,:Mid] = BQuarter[::-1,:,:] * np.array([-1,1,1])
                B[i][:Mid,Mid+1:,:Mid] = BQuarter[:,::-1,:] * np.array([1,-1,1])
                B[i][Mid+1:,Mid+1:,:Mid] = BQuarter[::-1,::-1,:] * np.array([-1,-1,1])
                B[i][:,:,Mid+1:] = B[i][:,:,:Mid]

            elif Coils[i].Plane == 'YZ' or Coils[i].Plane == 'ZY':

                B[i][:Mid,Mid+1:,:Mid] = BQuarter[:,::-1,:] * np.array([1,-1,1])
                B[i][:Mid,:Mid,Mid+1:] = BQuarter[:,:,::-1] * np.array([1,1,-1])
                B[i][:Mid,Mid+1:,Mid+1:] = BQuarter[:,::-1,::-1] * np.array([1,-1,-1])
                B[i][Mid+1:,:,:] = B[i][:Mid,:,:]

            elif Coils[i].Plane == 'XZ' or Coils[i].Plane == 'ZX':
                B[i][Mid+1:,:Mid,:Mid] = BQuarter[::-1,:,:] * np.array([-1,1,1])
                B[i][:Mid,:Mid,Mid+1:] = BQuarter[:,:,::-1] * np.array([1,1,-1])
                B[i][Mid+1:,:Mid,Mid+1:] = BQuarter[::-1,:,::-1] * np.array([-1,1,-1])
                B[i][:,Mid+1:,:] = B[i][:,:Mid,:]

        NetB = np.zeros((side,side,side,3))

        for i in range(len(Coils)):
            NetB += B[i]

        return NetB

    else:

        print("Sześcian symulacji musi mieć nieparzystą długość boku")
        
def InductionFromCircularCoil(Turns,Current,Radius):

    SimReach = 2*Radius
    X1 = np.arange(-SimReach,SimReach,0.01)
    B1 = np.zeros(len(X1))

    for i in range(len(X1)):
        B1[i] = const.mu_0 * Turns * Current * Radius**2 / 2 / (Radius**2 + X1[i]**2)**(3/2)

    X2 = -X1[1:]

    XC1 = np.concatenate((X1, np.flip(X2))) - Radius / 2
    BCC1 = np.concatenate((B1, np.flip(B1[1:])))

    XC2 = XC1 + Radius
    BCC2 = BCC1

    XC3 = np.arange(XC2[0], XC1[-1], 0.01)
    BCC3 = np.zeros(len(XC3))

    for i in range(len(XC1)):

        if XC1[i] >= XC2[0]:
            LeftIndex = i
            break

    for i in range(len(XC3)):
        BCC3[i] = BCC1[i + LeftIndex - 2] + BCC2[i]

    return XC3,BCC3

def InductionFromSquareCoil(I, a, R):
    cos_alfa = (0.5*a - R)/np.sqrt( (0.5*a)**2 + (0.5*a - R)**2)
    cos_beta = (0.5*a + R)/np.sqrt( (0.5*a)**2 + (0.5*a + R)**2)
    cos_gamma = (0.5*a)/np.sqrt( (0.5*a)**2 + (0.5*a + R)**2)
    cos_delta = (0.5*a)/np.sqrt( (0.5*a)**2 + (0.5*a - R)**2)

    return const.mu_0*I /4 /const.pi *(4*(cos_alfa +cos_beta)/a + 2*cos_delta/(0.5*a - R) + 2*cos_gamma/(0.5*a + R))
'''

def HomogeneityArea(B):

    avg = np.zeros(3)
    avg[0] = np.mean(np.abs(B[:,:,:,0]))
    avg[1] = np.mean(np.abs(B[:,:,:,1]))
    avg[2] = np.mean(np.abs(B[:,:,:,2]))

    std = np.zeros(3)

    for i in range(3):
        std[i] = np.std(np.abs(B[:,:,:,i]))
        #print(std[i])

    compatibility = np.zeros((len(B),len(B),len(B)))
    relativeStd = np.zeros(3)
    #relativeStd = 0

    #plt.figure()
    #ax = plt.subplot(1,1,1, projection='3d')

    for i in range(len(B)):
        for j in range(len(B)):
            for k in range(len(B)):
                for z in range(3):
                    relativeStd[z] = np.abs((B[i,j,k,z] - B[0,0,0,z])/B[0,0,0,z]) * 100
                #print(relativeStd)

                if relativeStd[0] <= 1 and relativeStd[1] <= 1 and relativeStd[2] <= 1:
                    compatibility[i, j, k] = 1
                    #ax.scatter(i - int(SimCubeSide / 2), j - int(SimCubeSide / 2), k - int(SimCubeSide / 2), 'o',color='blue')

    #plt.show()
    return compatibility

def RotateAlongX(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[1,0,0],[0,np.cos(angleRad),-np.sin(angleRad)],[0,np.sin(angleRad),np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OX

    return np.dot(RotationMatrix,vec)

def RotateAlongY(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),0,-np.sin(angleRad)],[0,1,0],[np.sin(angleRad),0 ,np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OY

    return np.dot(RotationMatrix,vec)

def RotateAlongZ(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),-np.sin(angleRad),0],[np.sin(angleRad),np.cos(angleRad),0],[0,0,1]]) #Defioniowanie macierzy obrotu o kąt, wokół OZ

    return np.dot(RotationMatrix,vec)

class HelmholtzCoils:  # Klasa generująca układ dwóch cewek Helmholtza o zadanych parametrach, kolejnymi argumentami
    # kostruktora są: płaszczyna w której leżą cewki ('XY','XZ','YZ'), ilość boków wielokąta foremnego cewki,
    # długość boku wielokąta cewki, ilość zwojów, średnica drutu, odległość między cewkami

    def __init__(self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance):

        self.Turns = NumberOfTurns
        self.WireGauge = WireGauge
        self.Distance = round(Distance,2)  # Inicjalizacja składowych klasy
        self.SideLength = Side
        self.Plane = Plane
        self.Polygon = NumberOfSides
        self.RotationAngle = 360/NumberOfSides
        self.InternalAngle = (180 * NumberOfSides - 360)/NumberOfSides


        if self.Plane == 'XY' or self.Plane == 'YX':
            self.Index = 2
        elif self.Plane == 'XZ' or self.Plane == 'ZX':
            self.Index = 1
        elif self.Plane == 'YZ' or self.Plane == 'ZY':
            self.Index = 0

    def ShowCoils(self):

        global dl

        plt.figure()
        ax = plt.subplot(1,1,1,projection = '3d')

        PointOnAxis = np.zeros((2,3))
        N = int(self.SideLength / dl)  # Ilość kroków na jeden bok

        for Coil in range(2):

            if self.Plane == 'XY' or self.Plane == 'YX':  # Definiowanie wektora przesuwającego punkt na osi, zadanie początkowego punktu na osi oraz na boku cewki

                PointOnAxis[0] = np.array([0, 0, -self.Distance / 2 ])
                PointOnAxis[1] = np.array([0, 0, self.Distance / 2 ])

                StartingPoint = np.array([self.SideLength / 2 , -self.SideLength / 2 * np.tan(self.InternalAngle / 360 * const.pi) ,0])

            elif self.Plane == 'XZ' or self.Plane == 'ZX':

                PointOnAxis[0] = np.array([0, -self.Distance / 2,0])
                PointOnAxis[1] = np.array([0, self.Distance / 2,0])

                StartingPoint = np.array([self.SideLength / 2 , 0, -self.SideLength / 2 * np.tan(self.InternalAngle / 360 * const.pi)])

            elif self.Plane == 'YZ' or self.Plane == 'ZY':

                PointOnAxis[0] = np.array([ -self.Distance / 2,0,0])
                PointOnAxis[1] = np.array([ self.Distance / 2,0,0])

                StartingPoint = np.array([0, self.SideLength / 2 , -self.SideLength / 2 * np.tan(self.InternalAngle / 360 * const.pi) ])

            PresentPoint = copy.deepcopy(StartingPoint)  # Powrót do punktu początkowego

            if self.Plane == 'XY' or self.Plane == 'YX':
                ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi), 0]) * dl

            elif self.Plane == 'XZ' or self.Plane == 'ZX':
                ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), 0, np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

            elif self.Plane == 'YZ' or self.Plane == 'ZY':
                ApproxVector = np.array([0, np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

            for Side in range(self.Polygon):

                for Step in range(N):
                    DistanceVector = PresentPoint - PointOnAxis[Coil]

                    if (Step % 10) == 0:

                        ax.scatter(DistanceVector[0]*100,DistanceVector[1]*100,DistanceVector[2]*100,marker='o',color='blue',s=1)

                    PresentPoint += ApproxVector  # Przesunięcie punktu na obwodzie cewki

                if self.Plane == 'XY' or self.Plane == 'YX':
                    ApproxVector = RotateAlongZ(ApproxVector, self.RotationAngle)

                elif self.Plane == 'XZ' or self.Plane == 'ZX':
                    ApproxVector = RotateAlongY(ApproxVector, self.RotationAngle)

                elif self.Plane == 'YZ' or self.Plane == 'ZY':
                    ApproxVector = RotateAlongX(ApproxVector, self.RotationAngle)

    def CalculateInduction(self, Current, x, y, z):  # Proponuję dodać wybór osi tutaj jako argument, np liczba oznaczająca przesunięsie wybranej osi od osi zerowej

        global dl, ProfileWidth, SimCubeSide # Krok dl aproksymacji numerycznej
        global ax

        PointOnAxis = np.zeros((2,3))

        B = np.zeros(3)

        N = int(self.SideLength / dl)  # Ilość kroków na jeden bok

        NetInductionForPoint = np.zeros(3)

        for Coil in range(2):

            if self.Plane == 'XY' or self.Plane == 'YX':  # Definiowanie wektora przesuwającego punkt na osi, zadanie początkowego punktu na osi oraz na boku cewki

                PointOnAxis[0] = np.array([0, 0, -self.Distance / 2 - z / 100 ])
                PointOnAxis[1] = np.array([0, 0, self.Distance / 2 - z / 100 ])

                StartingPoint = np.array([self.SideLength / 2 - x / 100, -self.SideLength / 2 * np.tan(self.InternalAngle / 360 * const.pi) - y/100,  0])

            elif self.Plane == 'XZ' or self.Plane == 'ZX':

                PointOnAxis[0] = np.array([0, -self.Distance / 2 - y/100,0])
                PointOnAxis[1] = np.array([0, self.Distance / 2 - y/100,0])

                StartingPoint = np.array([self.SideLength / 2 - x / 100, 0, -self.SideLength / 2 * np.tan(self.InternalAngle / 360 * const.pi) - z / 100])

            elif self.Plane == 'YZ' or self.Plane == 'ZY':

                PointOnAxis[0] = np.array([-self.Distance / 2 - x/100,0,0])
                PointOnAxis[1] = np.array([self.Distance / 2 - x/100,0,0])

                StartingPoint = np.array([0, self.SideLength / 2 - y / 100, -self.SideLength / 2 * np.tan(self.InternalAngle / 360 * const.pi) - z / 100])

            for CurrentTurn in range(self.Turns):  # Pętla odpowiadająca za kolejne zwoje

                NetInductionForCoil = np.zeros(3)

                PresentPoint = copy.deepcopy(StartingPoint)  # Powrót do punktu początkowego

                if self.Plane == 'XY' or self.Plane == 'YX':
                    ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi), 0]) * dl

                elif self.Plane == 'XZ' or self.Plane == 'ZX':
                    ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), 0, np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

                elif self.Plane == 'YZ' or self.Plane == 'ZY':
                    ApproxVector = np.array([0, np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

                DirectionVector = np.zeros(3)
                DirectionVector[self.Index] = 1

                for Side in range(self.Polygon):

                    for Step in range(N):
                        DistanceVector = PresentPoint + PointOnAxis[Coil] + DirectionVector * (CurrentTurn % math.floor(ProfileWidth/self.WireGauge)) * self.WireGauge  # Obliczanie wektora położenia dla punktu na cewce

                        NetInductionForCoil += BiotSavart(ApproxVector, DistanceVector, Current)  # Dodawanie kolejnych przyrostów indukcji
                        PresentPoint += ApproxVector  # Przesunięcie punktu na obwodzie cewki

                    if self.Plane == 'XY' or self.Plane == 'YX':
                        ApproxVector = RotateAlongZ(ApproxVector, self.RotationAngle)

                    elif self.Plane == 'XZ' or self.Plane == 'ZX':
                        ApproxVector = RotateAlongY(ApproxVector, self.RotationAngle)

                    elif self.Plane == 'YZ' or self.Plane == 'ZY':
                        ApproxVector = RotateAlongX(ApproxVector, self.RotationAngle)

            NetInductionForPoint += NetInductionForCoil

        B = NetInductionForPoint

        return B

def BiotSavart(DL, DistanceVector, Current):

    dB = const.mu_0 * Current / 4 / const.pi * np.cross(DL,DistanceVector) / np.linalg.norm(DistanceVector) ** 3
    # Formuła bez wersora, więc wartość wektora odległości musi być do potęgi trzeciej (tutaj był błąd)

    return dB

def MiddleValue(values):
    length = len(values)
    if (length%2):
        index = int(length/2 - 0.5)
        midValue = values[index]
    else:
        index = int(length/2)
        midValue = ( values[int(length/2)] +  values[int(length/2 - 1)])/2

    return midValue, index

def EvaluateInductionInVolume(B, Coils, Current, Xboundry1, Xboundry2, Yboundry1, Yboundry2, Zboundry1, Zboundry2):

    XSize = Xboundry2 - Xboundry1 + 1
    YSize = Yboundry2 - Yboundry1 + 1
    ZSize = Zboundry2 - Zboundry1 + 1

    B = np.zeros([XSize,YSize,ZSize,3])

    for i in range(XSize):
        for j in range(YSize):
            for k in range(ZSize):
                for z in range(3):
                    B[i,j,k] += Coils[z].CalculateInduction(Current,Xboundry1 + i, Yboundry1 + j, Zboundry2 + k)
    return B

dl = 0.01
Current = 1.3
ProfileWidth = 0.024

if __name__ == '__main__':

    SimCubeSide = 5
    Mid = int(SimCubeSide / 2)

    CS = []
    CS1 = HelmholtzCoils('XY', 8, 0.77, 30, 0.0012, 0.95)
    CS2 = HelmholtzCoils('YZ', 8, 0.77, 30, 0.0012, 0.95)
    CS3 = HelmholtzCoils('XZ', 8, 0.77, 30, 0.0012, 0.95)

    CS.append(CS1)
    CS.append(CS2)
    CS.append(CS3)

    pool = Pool(processes=6)
    B = np.zeros([Mid,Mid,Mid,3])
    BAbs = np.zeros([SimCubeSide, SimCubeSide, SimCubeSide, 3])
    results = []

    func = partial(EvaluateInductionInVolume,B,CS,Current)

    for i in range(2):
        for j in range(2):
            for k in range(2):
                #results.append(pool.apply_async(EvaluateInductionInVolume, args = (B0[i * Mid: Mid + i* Mid, j * Mid:Mid + j* Mid, k * Mid: Mid + k* Mid],CS, Current, -Mid + i * Mid, i * Mid,-Mid + j * Mid,j * Mid, -Mid + k * Mid,k * Mid)))
                results.append(pool.apply_async(func, args = (-Mid + i * Mid + i, i * Mid,-Mid + j * Mid + j,j * Mid, -Mid + k * Mid + k ,k * Mid)))

    pool.close()
    pool.join()

    Quarter = 0

    for i in range(2):
        for j in range(2):
            for k in range(2):
                BAbs[i * Mid + i: Mid + i * Mid + 1, j * Mid + j: Mid + j * Mid + 1, k * Mid + k: Mid + k * Mid + 1] =  results[Quarter].get()
                Quarter += 1

    IsItHomogeneous = HomogeneityArea(BAbs)
    ResultsFile = open('Helmholtz_Coils_induction.csv','w')

    with ResultsFile:

        writer = csv.writer(ResultsFile, delimiter = ' ')
        Header1 = ['Evaluated region dimensions: {}x{}x{} [cm]'.format(SimCubeSide-1,SimCubeSide-1,SimCubeSide-1)]
        writer.writerow(Header1)

        for Coil,i in zip(CS,range(1,len(CS) + 1)):
            writer.writerow(['Coil pair nr {}: N={}, a={}m, I={}A, d={}m, Turns={}, WireGauge={}mm, Plane:{}'.format(i,Coil.Polygon,Coil.SideLength,Current,Coil.Distance,Coil.Turns,Coil.WireGauge*1000,Coil.Plane)])

        writer.writerow(['X[cm],Y[cm],Z[cm],BX[T],BY[T],BZ[T],Qualified to homogeneous region'])

        for i in range(SimCubeSide):
            for j in range(SimCubeSide):
                for k in range(SimCubeSide):
                    writer.writerow(['{},{},{},{},{},{},{:d}'.format(i-Mid,j-Mid,k-Mid,BAbs[i,j,k,0],BAbs[i,j,k,1],BAbs[i,j,k,2],int(IsItHomogeneous[i,j,k]))])