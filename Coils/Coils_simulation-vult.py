import matplotlib.pyplot as plt
import numpy as np
from scipy import constants as const
import copy
import math

def HomogeneityArea(Coils1, Coils2, Coils3, Current1, Current2, Current3, side):

    B1 = np.zeros((side,side,side,3))
    B2 = np.zeros((side,side,side,3))
    B3 = np.zeros((side,side,side,3))



                    # Dodać warunek wybierający jeden z wariantów przypisania
                    # w zależności od płaszczyzny cewek

                    # Napisany wariant jest dobry dla nieparzystego oddalenia
                    # od osi, dopisać wariant dla parzystego

    if side % 2 == 1:

        progress = 0

        for i in range(int(side/2)+1):
            for j in range(int(side/2)+1):
                for k in range(int(side/2)+1):
                    for z in range(3):
                        B1[i,j,k] = Coils1.CalculateInduction(Current1,i - int(side/2),j - int(side/2),k - int(side/2))
                        B2[i,j,k] = Coils2.CalculateInduction(Current2,i - int(side/2),j - int(side/2),k - int(side/2))
                        B3[i,j,k] = Coils3.CalculateInduction(Current3,i - int(side/2),j - int(side/2),k - int(side/2))
                    progress += 1
                    if not (progress % 100):
                        print('Progres: {}%'.format(progress / (int(side/2)+1)**3 * 100))

        Bw = B1[:int(side/2)-1,:int(side/2)-1,:int(side/2)-1]

        B1[int(side/2):,:int(side/2),:int(side/2)] = Bw[::-1,:,:] * np.array([-1,1,1])
        B1[:int(side/2),int(side/2):,:int(side/2)] = Bw[:,::-1,:] * np.array([1,-1,1])
        B1[int(side/2):,int(side/2):,:int(side/2)] = Bw[::-1,::-1,:] * np.array([-1,-1,1])
        B1[:,:,int(side/2):] = B1[:,:,:int(side/2)-1]

        Bw = B2[:int(side/2)-1,:int(side/2)-1,:int(side/2)-1]

        B2[:int(side/2),int(side/2):,:int(side/2)] = Bw[:,::-1,:] * np.array([1,-1,1])
        B2[:int(side/2),:int(side/2),int(side/2):] = Bw[:,:,::-1] * np.array([1,1,-1])
        B2[:int(side/2),int(side/2):,int(side/2):] = Bw[:,::-1,::-1] * np.array([1,-1,-1])
        B2[int(side/2):,:,:] = B2[:int(side/2)-1,:,:]

        Bw = B3[:int(side/2)-1,:int(side/2)-1,:int(side/2)-1]

        B3[int(side/2):,:int(side/2),:int(side/2)] = Bw[::-1,:,:] * np.array([-1,1,1])
        B3[:int(side/2),:int(side/2),int(side/2):] = Bw[:,:,::-1] * np.array([1,1,-1])
        B3[int(side/2):,:int(side/2),int(side/2):] = Bw[::-1,:,::-1] * np.array([-1,1,-1])
        B3[:,int(side/2):,:] = B3[:,:int(side/2)-1,:]

        return B1 + B2 + B3

    else:

        for i in range(int(side/2)):
            for j in range(int(side/2)):
                for k in range(int(side/2)):
                    for z in range(3):
                        B1[i,j,k] = Coils1.CalculateInduction(Current1,i - int(side/2),j - int(side/2),k - int(side/2))
                        B2[i,j,k] = Coils2.CalculateInduction(Current2,i - int(side/2),j - int(side/2),k - int(side/2))
                        B3[i,j,k] = Coils3.CalculateInduction(Current3,i - int(side/2),j - int(side/2),k - int(side/2))

        Bw = B1[:int(side/2),:int(side/2),:int(side/2)]

        B1[int(side/2):,:int(side/2),:int(side/2)] = Bw[::-1,:,:] * np.array([-1,1,1])
        B1[:int(side/2),int(side/2):,:int(side/2)] = Bw[:,::-1,:] * np.array([1,-1,1])
        B1[int(side/2):,int(side/2):,:int(side/2)] = Bw[::-1,::-1,:] * np.array([-1,-1,1])
        B1[:,:,int(side/2):] = B1[:,:,:int(side/2)]

        Bw = B2[:int(side/2),:int(side/2),:int(side/2)]

        B2[:int(side/2),int(side/2):,:int(side/2)] = Bw[:,::-1,:] * np.array([1,-1,1])
        B2[:int(side/2),:int(side/2),int(side/2):] = Bw[:,:,::-1] * np.array([1,1,-1])
        B2[:int(side/2),int(side/2):,int(side/2):] = Bw[:,::-1,::-1] * np.array([1,-1,-1])
        B2[int(side/2):,:,:] = B2[:int(side/2),:,:]

        Bw = B3[:int(side/2),:int(side/2),:int(side/2)]

        B3[int(side/2):,:int(side/2),:int(side/2)] = Bw[::-1,:,:] * np.array([-1,1,1])
        B3[:int(side/2),:int(side/2),int(side/2):] = Bw[:,:,::-1] * np.array([1,1,-1])
        B3[int(side/2):,:int(side/2),int(side/2):] = Bw[::-1,:,::-1] * np.array([-1,1,-1])
        B3[:,int(side/2):,:] = B3[:,:int(side/2),:]

        return B1 + B2 + B3


def RotateAlongX(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[1,0,0],[0,np.cos(angleRad),-np.sin(angleRad)],[0,np.sin(angleRad),np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OX

    return np.dot(RotationMatrix,vec)

def RotateAlongY(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),0,-np.sin(angleRad)],[0,1,0],[np.sin(angleRad),0 ,np.cos(angleRad)]]) #Defioniowanie macierzy obrotu o kąt, wokół OY

    return np.dot(RotationMatrix,vec)

def RotateAlongZ(vec,angleDeg):

    angleRad = angleDeg/360 * 2 * const.pi

    RotationMatrix = np.array([[np.cos(angleRad),-np.sin(angleRad),0],[np.sin(angleRad),np.cos(angleRad),0],[0,0,1]]) #Defioniowanie macierzy obrotu o kąt, wokół OZ

    return np.dot(RotationMatrix,vec)

class HelmholtzCoils:  # Klasa generująca układ dwóch cewek Helmholtza o zadanych parametrach, kolejnymi argumentami
    # kostruktora są: płaszczyna w której leżą cewki ('XY','XZ','YZ'), kształt cewek, na chwilę obecną 'octagonal'
    # oraz 'asquare', długość boku wielokąta cewki, ilość zwojów, średnica drutu, odległość między cewkami

    def __init__(self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance):

        self.Turns = NumberOfTurns
        self.WireGauge = WireGauge
        self.Distance = round(Distance,2)  # Inicjalizacja składowych klasy
        self.SideLength = Side
        self.Plane = Plane
        self.Polygon = NumberOfSides
        self.RotationAngle = 360/NumberOfSides
        self.InternalAngle = (180 * NumberOfSides - 360)/NumberOfSides


        if self.Plane == 'XY' or self.Plane == 'YX':
            self.Index = 2
        elif self.Plane == 'XZ' or self.Plane == 'ZX':
            self.Index = 1
        elif self.Plane == 'YZ' or self.Plane == 'ZY':
            self.Index = 0

    def CalculateInduction(self, Current, x, y, z):  # Proponuję dodać wybór osi tutaj jako argument, np liczba oznaczająca przesunięsie wybranej osi od osi zerowej

        global dl, ProfileWidth, SimCubeSide  # Krok dl aproksymacji numerycznej

        PointOnAxis = np.zeros((2,3))

        B = np.zeros(3)

        N = int(self.SideLength / dl)  # Ilość kroków na jeden bok

        NetInductionForPoint = np.zeros(3)

        for Coil in range(2):

            if self.Plane == 'XY' or self.Plane == 'YX':  # Definiowanie wektora przesuwającego punkt na osi, zadanie początkowego punktu na osi oraz na boku cewki

                PointOnAxis[Coil] = np.array([0 , 0, -self.Distance/2 - z/100 + Coil * 2 * z/100])
                StartingPoint = np.array([self.SideLength / 2 - x/100, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi), 0])

            elif self.Plane == 'XZ' or self.Plane == 'ZX':

                PointOnAxis[Coil] = np.array([0, -self.Distance/2 - y/100 + Coil * 2 * y/100, 0])
                StartingPoint = np.array([self.SideLength / 2 - x/100, 0, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi) - z/100])

            elif self.Plane == 'YZ' or self.Plane == 'ZY':

                PointOnAxis[Coil] = np.array([-self.Distance/2 - x/100 + Coil * 2 * x/100, 0, 0])
                StartingPoint = np.array([0, self.SideLength / 2 - y/100, -self.SideLength/2 * np.tan(self.InternalAngle / 360 * const.pi) - z/100])


            for CurrentTurn in range(self.Turns):  # Pętla odpowiadająca za kolejne zwoje

                NetInductionForCoil = np.zeros(3)

                PresentPoint = copy.deepcopy(StartingPoint)  # Powrót do punktu początkowego

                if self.Plane == 'XY' or self.Plane == 'YX':
                    ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi), 0]) * dl

                elif self.Plane == 'XZ' or self.Plane == 'ZX':
                    ApproxVector = np.array([np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), 0, np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

                elif self.Plane == 'YZ' or self.Plane == 'ZY':
                    ApproxVector = np.array([0, np.cos((180 - self.InternalAngle)/360 * 2 * const.pi), np.sin((180 - self.InternalAngle)/360 * 2 * const.pi)]) * dl

                DirectionVector = np.zeros(3)
                DirectionVector[self.Index] = 1

                for Side in range(self.Polygon):

                    for Step in range(N):
                        DistanceVector = PointOnAxis[Coil] + PresentPoint + DirectionVector * (CurrentTurn % math.floor(ProfileWidth/self.WireGauge)) * self.WireGauge  # Obliczanie wektora położenia dla punktu na cewce

                        NetInductionForCoil += BiotSavart(ApproxVector, DistanceVector, Current)  # Dodawanie kolejnych przyrostów indukcji

                        PresentPoint += ApproxVector  # Przesunięcie punktu na obwodzie cewki

                    if self.Plane == 'XY' or self.Plane == 'YX':
                        ApproxVector = RotateAlongZ(ApproxVector, self.RotationAngle)

                    elif self.Plane == 'XZ' or self.Plane == 'ZX':
                        ApproxVector = RotateAlongY(ApproxVector, self.RotationAngle)

                    elif self.Plane == 'YZ' or self.Plane == 'ZY':
                        ApproxVector = RotateAlongX(ApproxVector, self.RotationAngle)

            NetInductionForPoint += NetInductionForCoil

        B = NetInductionForPoint

        return B

def BiotSavart(DL, DistanceVector, Current):

    dB = const.mu_0 * Current / 4 / const.pi * np.cross(DL,DistanceVector) / np.linalg.norm(DistanceVector) ** 3
    # Formuła bez wersora, więc wartość wektora odległości musi być do potęgi trzeciej (tutaj był błąd)

    return dB

def InductionFromCircularCoil(Turns,Current,Radius):

    SimReach = 2*Radius
    X1 = np.arange(-SimReach,SimReach,0.01)
    B1 = np.zeros(len(X1))

    for i in range(len(X1)):
        B1[i] = const.mu_0 * Turns * Current * Radius**2 / 2 / (Radius**2 + X1[i]**2)**(3/2)

    X2 = -X1[1:]

    XC1 = np.concatenate((X1, np.flip(X2))) - Radius / 2
    BCC1 = np.concatenate((B1, np.flip(B1[1:])))

    XC2 = XC1 + Radius
    BCC2 = BCC1

    XC3 = np.arange(XC2[0], XC1[-1], 0.01)
    BCC3 = np.zeros(len(XC3))

    for i in range(len(XC1)):

        if XC1[i] >= XC2[0]:
            LeftIndex = i
            break

    for i in range(len(XC3)):
        BCC3[i] = BCC1[i + LeftIndex - 2] + BCC2[i]

    return XC3,BCC3

def InductionFromSquareCoil(I, a, R):
    cos_alfa = (0.5*a - R)/np.sqrt( (0.5*a)**2 + (0.5*a - R)**2)
    cos_beta = (0.5*a + R)/np.sqrt( (0.5*a)**2 + (0.5*a + R)**2)
    cos_gamma = (0.5*a)/np.sqrt( (0.5*a)**2 + (0.5*a + R)**2)
    cos_delta = (0.5*a)/np.sqrt( (0.5*a)**2 + (0.5*a - R)**2)

    return const.mu_0*I /4 /const.pi *(4*(cos_alfa +cos_beta)/a + 2*cos_delta/(0.5*a - R) + 2*cos_gamma/(0.5*a + R))

def MiddleValue(values):
    length = len(values)
    if (length%2):
        index = int(length/2 - 0.5)
        midValue = values[index]
    else:
        index = int(length/2)
        midValue = ( values[int(length/2)] +  values[int(length/2 - 1)])/2

    return midValue, index


dl = 0.01
Current = 1.3
ProfileWidth = 0.024
SimCubeSide = 2  # [cm]

CS1 = HelmholtzCoils('XY', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)
CS2 = HelmholtzCoils('YZ', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)
CS3 = HelmholtzCoils('XZ', 8, 0.77, 1, 0.0012, 0.95) # (self, Plane, NumberOfSides, Side, NumberOfTurns, WireGauge, Distance)

CS = []
CS.append(CS1)
CS.append(CS2)
CS.append(CS3)

for i in range(-3,4,1):
    b = CS1.CalculateInduction(Current,i,0,0)
    plt.plot(i,b[0],'.r')

plt.show()
#bb = []
#rr = []


#B1 = CS1.CalculateInduction(Current,0,0,0)
#B2 = CS2.CalculateInduction(Current,0,0,0)
#B3 = CS3.CalculateInduction(Current,0,0,0)

#Bw = B1 + B2 + B3

SimCubeSide = 5

B10 = HomogeneityArea(CS1,CS2,CS3,Current,Current,Current,SimCubeSide)



B4 = np.zeros((SimCubeSide,SimCubeSide,SimCubeSide,3))

for i in range(int(SimCubeSide)):
    for j in range(int(SimCubeSide)):
        for k in range(int(SimCubeSide)):
            for z in range(3):
                B4[i,j,k] += CS[z].CalculateInduction(Current,i - int(SimCubeSide/2),j - int(SimCubeSide/2),k - int(SimCubeSide/2))

print(B10/B4)
#print(B/B4)
