import matplotlib.pyplot as plt
import numpy as np
from scipy import constants as const


class HelmholtzCoils:  # Klasa generująca układ dwóch cewek Helmholtza o zadanych parametrach, kolejnymi argumentami
    # kostruktora są: płaszczyna w której leżą cewki ('XY','XZ','YZ'), kształt cewek, na chwilę obecną 'octagonal'
    # oraz 'square', długość boku wielokąta cewki, ilość zwojów, średnica drutu, odległość między cewkami

    def __init__(self, Plane, Shape, Side, NumberOfTurns, WireGauge, Distance):

        self.Turns = NumberOfTurns
        self.WireGauge = WireGauge
        self.Distance = Distance  # Inicjalizacja składowych klasy
        self.SideLength = Side
        self.Plane = Plane
        self.Shape = Shape

        if Shape == 'octagonal':

            a1 = np.cos(45 / 360 * 2 * const.pi)
            a2 = np.sin(45 / 360 * 2 * const.pi)

            if Plane == 'XY':

                VECTORS = np.zeros((8, 3))

                VECTORS[0] = np.array([a1, a2, 0])
                VECTORS[1] = np.array([0, 1, 0])
                VECTORS[2] = np.array([-a1, a2, 0])
                VECTORS[3] = np.array([-1, 0, 0])  # Kreacja kolejnych wektorów jednostkowych, które będą odpowiadały
                # za 'obrót' wektora wskazującego na kolejne fragmenty dl cewki

                VECTORS[4] = np.array([-a1, -a2, 0])
                VECTORS[5] = np.array([0, -1, 0])
                VECTORS[6] = np.array([a1, -a2, 0])
                VECTORS[7] = np.array([1, 0, 0])

                self.UnitVectors = VECTORS

            elif Plane == 'XZ':

                VECTORS = np.zeros((8, 3))

                VECTORS[0] = np.array([a1, 0, a2])
                VECTORS[1] = np.array([0, 0, 1])
                VECTORS[2] = np.array([-a1, 0, a2])
                VECTORS[3] = np.array([-1, 0, 0])
                VECTORS[4] = np.array([-a1, 0, -a2])
                VECTORS[5] = np.array([0, 0, -1])
                VECTORS[6] = np.array([a1, 0, -a2])
                VECTORS[7] = np.array([1, 0, 0])

                self.UnitVectors = VECTORS

            elif Plane == 'YZ':

                VECTORS = np.zeros((8, 3))

                VECTORS[0] = np.array([0, a1, a2])
                VECTORS[1] = np.array([0, 0, 1])
                VECTORS[2] = np.array([0, -a1, a2])
                VECTORS[3] = np.array([0, -1, 0])
                VECTORS[4] = np.array([0, -a1, -a2])
                VECTORS[5] = np.array([0, 0, -1])
                VECTORS[6] = np.array([0, a1, -a2])
                VECTORS[7] = np.array([0, 1, 0])

                self.UnitVectors = VECTORS

        elif Shape == 'square':

            if Plane == 'XY':

                VECTORS = np.zeros((4, 3))

                VECTORS[0] = [0, 1, 0]
                VECTORS[1] = [-1, 0, 0]
                VECTORS[2] = [0, -1, 0]
                VECTORS[3] = [1, 0, 0]

                self.UnitVectors = VECTORS

            elif Plane == 'XZ':

                VECTORS = np.zeros((4, 3))

                VECTORS[0] = [0, 0, 1]
                VECTORS[1] = [-1, 0, 0]
                VECTORS[2] = [0, 0, -1]
                VECTORS[3] = [1, 0, 0]

                self.UnitVectors = VECTORS

            elif Plane == 'YZ':

                VECTORS = np.zeros((4, 3))

                VECTORS[0] = [0, 0, 1]
                VECTORS[1] = [-1, 0, 0]
                VECTORS[2] = [0, 0, -1]
                VECTORS[3] = [1, 0, 0]

                self.UnitVectors = VECTORS

    def CalculateInduction(self, Current):  # Proponuję dodać wybór osi tutaj jako argument, np liczba oznaczająca przesunięsie wybranej osi od osi zerowej

        global dl #Krok dl aproksymacji numerycznej
        AxisStep = 0.01 #Krok na osi względem której obliczane jest pole
        SimReach = 2 * self.Distance #Zasięg symulacji, oznacza to, że obliczone zostanie pole w odległości 2*dystans między cewkami, wokół każdej z cewek

        if self.Shape == 'octagonal':

            if self.Plane == 'XY' or 'YX': #Definiowanie wektora przesuwającego punkt na osi, zadanie początkowego punktu na osi oraz na boku cewki
                AxisStepVector = np.array([0, 0, AxisStep])
                PointOnAxis = np.array([0, 0, float(-SimReach)])
                StartingPoint = np.array([self.SideLength / 2, -self.SideLength / 2 - self.SideLength * np.sin(45 / 360 * 2 * const.pi), 0.])

            elif self.Plane == 'XZ' or 'ZX':
                AxisStepVector = np.array([0, AxisStep, 0])
                PointOnAxis = np.array([0, float(-SimReach), 0])
                StartingPoint = np.array([self.SideLength / 2, 0., -self.SideLength / 2 - self.SideLength * np.sin(45 / 360 * 2 * const.pi)])

            else:
                AxisStepVector = np.array([AxisStep, 0, 0])
                PointOnAxis = np.array([float(-SimReach), 0, 0])
                StartingPoint = np.array([0., self.SideLength / 2, -self.SideLength / 2 - self.SideLength * np.sin(45 / 360 * 2 * const.pi)])

        elif self.Shape == 'square':

            if self.Plane == 'XY' or 'YX':

                AxisStepVector = np.array([0, 0, AxisStep])
                PointOnAxis = np.array([0, 0, float(-SimReach)])
                StartingPoint = np.array([self.SideLength / 2, -self.SideLength / 2 , 0.])

            elif self.Plane == 'XZ' or 'ZX':

                AxisStepVector = np.array([0, AxisStep, 0])
                PointOnAxis = np.array([0, float(-SimReach), 0])
                StartingPoint = np.array([self.SideLength / 2, 0., -self.SideLength / 2])

            else:

                AxisStepVector = np.array([AxisStep, 0, 0])
                AxisStepVector = np.array([AxisStep, 0, 0])
                StartingPoint = np.array([0., self.SideLength / 2, -self.SideLength / 2])



        N = int(self.SideLength / dl) #Ilość kroków na jeden bok

        R = np.zeros(int(SimReach / AxisStep))
        B = np.zeros(int(SimReach / AxisStep))

        for j in range(len(B)):

            PresentPoint = StartingPoint
            NetInductionForPoint = np.zeros(3)

            for CurrentTurn in range(self.Turns): #Pętla odpowiadająca za kolejne zwoje

                PresentPoint = StartingPoint #Powrót do punktu początkowego

                for Section in range(len(self.UnitVectors)): #Pętla odpowiedzialna za obliczanie dla każdego boku cewki

                    PresentApproxVector = self.UnitVectors[Section] * dl #Definiowanie wektora 'przesuwającego' punkt po cewce

                    for Step in range(N):
                        DistanceVector = PresentPoint + PointOnAxis + PointOnAxis / np.linalg.norm(
                            PointOnAxis) * self.WireGauge * CurrentTurn #Obliczanie wektora położenia dla punktu na cewce
                        #względem punktu na osi

                        NetInductionForPoint += BioteSavart(PresentApproxVector, DistanceVector, Current) #Dodawanie kolejnych przyrostów indukcji

                        PresentPoint += PresentApproxVector #Przesunięcie punktu na obwodzie cewki

            if self.Plane == 'XY' or 'YX':
                R[j] = PointOnAxis[2]

            elif self.Plane == 'XZ' or 'ZX':
                R[j] = PointOnAxis[1]

            else:
                R[j] = PointOnAxis[0]

            PointOnAxis += AxisStepVector
            B[j] = np.linalg.norm(NetInductionForPoint)

        R2 = -R

        RC1 = np.concatenate((R, np.flip(R2))) - self.Distance / 2
        BC1 = np.concatenate((B, np.flip(B)))

        RC2 = RC1 + self.Distance
        BC2 = BC1

        RC3 = np.arange(RC2[0], RC1[-1], AxisStep)
        BC3 = np.zeros(len(RC3))

        print(len(RC3))
        print(len(BC3))

        for i in range(len(RC1)):

            if RC1[i] >= RC2[0]:
                LeftIndex = i
                break

        for i in range(len(RC3)):
            BC3[i] = BC1[i + LeftIndex - 1] + BC2[i]

        return RC3, BC3


def BioteSavart(DL, DistanceVector, Current):
    dB = const.mu_0 * Current / 4 / const.pi * np.cross(DL, DistanceVector) / np.linalg.norm(DistanceVector) ** 2

    return dB

dl = 0.01

CS = HelmholtzCoils('XY', 'octagonal', 1.4, 2, 0.0015, 1.825)

[R, B] = CS.CalculateInduction(3.46)

plt.plot(R,B)
plt.show()